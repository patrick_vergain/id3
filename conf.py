# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# http://www.sphinx-doc.org/en/master/config
# -- Path setup --------------------------------------------------------------
# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))
import platform
from datetime import datetime
from zoneinfo import ZoneInfo

import sphinx
import sphinx_material

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
# sys.path.insert(0, os.path.abspath("./src"))

project = "id3_work"
html_title = project

author = f"Patrick Vergain"
html_logo = "images/python-logo.png"
html_favicon = "images/python-logo.png"
release = "0.1.0"
now = datetime.now(tz=ZoneInfo("Europe/Paris"))
version = f"{now.year}-{now.month:02}-{now.day:02} {now.hour:02}H ({now.tzinfo})"
today = version

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.doctest",
    "sphinx.ext.extlinks",
    "sphinx.ext.intersphinx",
    "sphinx.ext.todo",
    "sphinx.ext.mathjax",
    "sphinx.ext.viewcode",
    "sphinx_copybutton",
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]
html_static_path = ["_static"]
html_show_sourcelink = True
html_sidebars = {
    "**": ["logo-text.html", "globaltoc.html", "localtoc.html", "searchbox.html"]
}
extensions.append("sphinx_material")
html_theme_path = sphinx_material.html_theme_path()
html_context = sphinx_material.get_html_context()
html_theme = "sphinx_material"

extensions.append("sphinx.ext.intersphinx")
intersphinx_mapping = {
    "docker": ("https://gdevops.frama.io/tuto_docker/", None),
    "django": ("https://gdevops.frama.io/tuto_django/", None),
    "documentation": ("https://gdevops.frama.io/tuto_documentation/", None),
    "http": ("https://gdevops.frama.io/tuto_http/", None),
    "webframeworks": ("https://gdevops.frama.io/tuto_webframeworks/", None),
    "project": ("https://gdevops.frama.io/opsindev/tuto-project/", None),
    "webframeworks": ("https://gdevops.frama.io/tuto_webframeworks/", None),
    "conda": ("https://conda.io/en/latest/", None),
    "sphinx": ("https://www.sphinx-doc.org/en/master", None),
    "sphinx_material": ("https://gdevops.frama.io/tuto_sphinx_material", None),
}
extensions.append("sphinx.ext.todo")
todo_include_todos = True


# material theme options (see theme.conf for more information)
# https://gitlab.com/bashtage/sphinx-material/blob/master/sphinx_material/sphinx_material/theme.conf
# Colors
# The theme color for mobile browsers. Hex color.
# theme_color = #3f51b5
# Primary colors:
# red, pink, purple, deep-purple, indigo, blue, light-blue, cyan,
# teal, green, light-green, lime, yellow, amber, orange, deep-orange,
# brown, grey, blue-grey, white
# Accent colors:
# red, pink, purple, deep-purple, indigo, blue, light-blue, cyan,
# teal, green, light-green, lime, yellow, amber, orange, deep-orange
# color_accent = blue
# color_primary = blue-grey

# material theme options (see theme.conf for more information)
html_theme_options = {
    "base_url": "https://patrick_vergain.frama.io/id3/",
    "repo_url": "https://framagit.org/patrick_vergain/id3",
    "repo_name": project,
    "html_minify": False,
    "html_prettify": True,
    "css_minify": True,
    "repo_type": "gitlab",
    "globaltoc_depth": -1,
    "color_primary": "green",
    "color_accent": "cyan",
    "theme_color": "#2196f3",
    "nav_title": f"{project} ({today})",
    "master_doc": False,
    "nav_links": [
        {
            "href": "genindex",
            "internal": True,
            "title": "Index",
        },
        {
            "href": "https://gdevops.frama.io/tuto_sphinx_material/",
            "internal": False,
            "title": "Sphinx-material example",
        },
        {
            "href": "https://gdevops.frama.io/tuto_devops/",
            "internal": False,
            "title": "Tutos devops",
        },
    ],
    "heroes": {
        "index": "Travail chez id3",
    },
    "table_classes": ["plain"],
}


language = "en"
html_last_updated_fmt = ""

todo_include_todos = True

html_use_index = True
html_domain_indices = True

copyright = f"2018-{now.year}, {author} Built with sphinx {sphinx.__version__} Python {platform.python_version()}"
