
.. index::
   pair: 2014 ; Semaine 14 (Fin-Mars début avril 2014)


.. _semaine_14_2014:

==============================================================
Weekly semaine 14 2014
==============================================================


.. seealso::

   - :ref:`temps_passe_gestion_projets`

.. contents::
   :depth: 3



Lundi 31 mars 2014
======================

Mise en place des logiciels sur la machine Windows7 (:ref:`uc001`)
-------------------------------------------------------------------

- LibreOffice 4.2 (LibreOffice_4.2.1_Win_x86)


Travail sur P4N307
-------------------

