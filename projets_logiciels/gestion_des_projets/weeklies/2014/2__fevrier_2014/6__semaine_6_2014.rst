
.. index::
   pair: Mafia; Eurocopter
   pair: 2014 ; Semaine 6


.. _semaine6_2014:

==============================================================
Mes activités à id3 Technologies semaine 6 2014
==============================================================


.. contents::
   :depth: 3


Mercredi 5 février 2014
========================


PDEV1S173 CEA Imageur
----------------------

Modification du :ref:`projet CEA Imageur <pdev1s173_cea_imageur>` : pilotage d'une carte permettant d'allumer/éteindre des LEDs.


Projet PDEV4N307 TestCapteurs
-----------------------------------

Projet :ref:`Test des capteurs <pdev4n307_test_capteurs>`.

Hamza découvre que la balance d'Eugène permet d'envoyer par USB des mesures
de poids en grammes.



Projet PDEV2X238 Eurocopter (Mafia)
-----------------------------------

Frédéric revient :ref:`d'Eurocopter <pdev2x238_eurocopter>`

Les mafieux exigent de faire des modifications sur un produit périmé (le PDA qui
va être abandonné au profit de tablettes Android) sinon ils ne payent pas les 30000 euros !


Mardi 4 février 2014
=====================

J'aide :ref:`Yannick IDELON <yannick_idelon>` à mettre en place:

- des environnements virtuels Python 
- Eclipse avec PyDev


