
.. index::
   pair: 2014 ; Semaine 16 (14 -> 18 avril)


.. _semaine_16_2014:

==============================================================
Weekly semaine 16 du 14 au 18 avril 2014
==============================================================

.. seealso::

   - :ref:`temps_passe_gestion_projets`

.. contents::
   :depth: 3


Lundi 14 avril 2014 (C:\projects_id3\P3N161\DLLs_BioAPI\1.0.0\id3BioAPI, Windows 7.0)
======================================================================================


- passage du projet Visual C++ à Visual Studio Express sous Windows 7.
- début enregistrement BSP Morpho Top.

.. figure:: bioapi_registry_manager.png
   :align: center


