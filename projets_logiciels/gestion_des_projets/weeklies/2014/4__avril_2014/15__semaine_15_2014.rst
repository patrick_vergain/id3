
.. index::
   pair: 2014 ; Semaine 15 (Fin-Mars début avril 2014)


.. _semaine_15_2014:

==============================================================
Weekly semaine 15 2014
==============================================================


.. seealso::

   - :ref:`temps_passe_gestion_projets`

.. contents::
   :depth: 3



Lundi 7 avril 2014
======================

Poursuite en place des logiciels sur la machine Windows7 (:ref:`uc001`)
------------------------------------------------------------------------



Travail sur P4N307
-------------------


Auto-formation
--------------

- C++
- ipython
- python


Travail sur CL1356
-------------------

