
.. index::
   pair: id3 Technologies News; Nom
   single: id3 Technologies
   pair; id3 Technologies; 8 août 2012


.. _news_8_aout_2012:

===========================================================
Mercredi 8 août 2012 Id3 Semiconductors-> Id3 Technologies
===========================================================

.. seealso:: http://www.id3.eu/fr/press_0/press.htm


.. figure:: changement_nom.png

   id3 Semiconductors devient id3 Technologies


08 - 08 - 2012 - id3 change de nom : id3 Semiconductors devient id3 Technologies
=================================================================================


La société « id3 Semiconductors » change sa dénomination sociale et s’appelle
désormais « id3 Technologies ».

« Ce changement reflète l'évolution stratégique entreprise ces dernières années »,
déclare Jean-Louis Revol, PDG et co-fondateur d’id3.

« Nous offrons à nos clients des solutions de pointe pour les aider à résoudre
leurs défis technologiques. Nous continuons également d'investir en R&D pour
développer notre offre produit : signature électronique, solution d' AFIS,
nouvelle génération de SDK biométrique, RFID longue distance de lecture, ...

Pour ne citer que les derniers produits en date. Le terme de « Technologies »
reflète plus justement la culture de l'innovation d’id3 ».

A propos d’id3 Technologies
===========================

Fondée en 1990 par des ingénieurs expérimentés issus de ST Microelectronics,
id3 Technologies (alors “id3 Semiconductors") a développé de nombreuses
innovations et connu de nombreux succès.

Développant des solutions de reconnaissance d'empreintes digitales et de
technologies de lecture sans contacts depuis plus de 15 ans, id3 bénéficie
d’un savoir-faire reconnu mondialement et de partenariats solides.

Sa réputation est due à son rôle de précurseur de la technologie Match On Card
et aux performances obtenues lors des certifications MINEX - ses algorithmes
biométriques sont parmi les mieux classés au monde.

Sur le terrain, id3 a su prouver son efficacité et sa réactivité lors de projet
complexes dans des conditions environnementales difficiles, là où d'autres
ont échoué.

Loin de se reposer sur ses lauriers, id3 Technologies fournit encore et
toujours un effort constant pour améliorer ses technologies,  apporter à ses
clients les meilleures solutions aptes à relever les défis technologiques
auxquels ils font face et poursuivre son implantation mondiale.

