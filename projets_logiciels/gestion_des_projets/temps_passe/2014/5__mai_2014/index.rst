
.. index::
   pair: Temps passé ; Gestion des projets (2014)
   pair: Note de service; 5 mai 2014



.. _temps_5_mai_2014:

==============================================
Note de service : FICHE DE TEMPS (5 mai 2014)
==============================================


.. figure:: annee_2014.png
   :align: center

::

    Return-Path: 	<sylvie.ceriolo@id3.eu>
    Date: 	Mon, 05 May 2014 09:08:36 +0200
    From: 	Sylvie CERIOLO <sylvie.ceriolo@id3.eu>
    To: 	tout_id3 <tout_id3@id3.eu>
    Subject: 	Note de service : FICHE DE TEMPS


Vous trouverez sous vos users dans le répertoire FDT, des fiches de temps format 
excel à remplir.

Nous revenons à l'ancien système pour des raisons de gain de temps et de gestion.

Tout le monde doit remplir ses fiches.

Attention : une journée d'absence vaut 8h et 1/2 journée 4h.

Ne pas modifier le format, ni les cases, on verra au fur et à mesure les 
modifications à faire pour que toutes les fiches restent identiques.

Ceux qui ont rempli leur fiche de temps sur l'intranet n'ont pas besoin de 
recommencer, par contre ceux qui n'ont rien fait ou pas grand chose, merci de 
mettre à jour vos fiches de temps excel.

Voir avec vos chefs de service pour tous problèmes.

Merci.
