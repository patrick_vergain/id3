

.. index::
   pair: Projets CEA; Wavelens


.. _pdev3Y071_cea_wavelens:

===================================================
PDEV3Y071 CEA Wavelens (26 novembre 2013-
===================================================


.. seealso::

   - file:///C:/projects_id3/P3Y071/Soft_PC/XLOG3Y388_GUI_WavelensDemo/doc_sphinx/build/html/index.html


.. toctree::
   :maxdepth: 3


Dépot subversion
================

- svn+ssh://pvergain@svn/export/disk0/svn/P3Y071




Gestion de projet
==================

.. toctree::
   :maxdepth: 3

   gestion_de_projet/gestion_de_projet
