

.. index::
   pair: Projets; PDEV1S173
   pair: Projets; CEA Imageur
   pair: Projets CEA; Imageur
   pair: Projets; C♯
   pair: Projets; C♯4
   pair: GUI; XLOG1Z322_GraphicalUserInterface
   pair: Bitbucket; videocell (PDEV1S173)


.. _pdev1s173_cea_imageur:

======================
PDEV1S173 CEA Imageur
======================


.. seealso::

   - file:///C:/projects_id3/P1S173/Videocell/XLOG1Z322_GraphicalUserInterface/docs/doc_sphinx/_build/html/index.html

.. contents::
   :depth: 2



.. _bitbucket_videocell:

Bitbucket project
=================

.. seealso::

   - https://bitbucket.org/pvergain/videocell

Environnement logiciel
======================

- Le projet est écrit en C♯ (C♯4)
- L'environnement de développement utilisé est Visual Studio 2012.



Synonymes
=========

- projet ``VideoCell``


Dépot subversion
================

- :file:`/export/disk0/svn/P1S173/Concept/Soft/XLOG1Z322_GraphicalUserInterface`
- svn+ssh://pvergain@svn/export/disk0/svn/P1S173


Projet C♯  XLOG1Z322
=====================

- :file:`Y:\P1S173_VideoCell\Soft\XLOG1Z322_GraphicalUserInterface\project\VideoCell.sln`


Gestion de projet
==================

.. toctree::
   :maxdepth: 3
   
   gestion_de_projet/index
   
   
