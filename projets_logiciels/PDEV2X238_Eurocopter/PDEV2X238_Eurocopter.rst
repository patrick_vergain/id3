

.. index::
   pair: Projets; PDEV2X238
   pair: Projets; Eurocopter

.. _pdev2x238_eurocopter:

======================
PDEV2X238 Eurocopter
======================



.. seealso::

   - http://fr.wikipedia.org/wiki/Eurocopter
   - http://fr.wikipedia.org/wiki/Lutz_Bertling
   - http://www.eurocopter.com/site/en/ref/home.html
   - :ref:`eurocopter`
   - :ref:`eads`


.. contents::
   :depth: 2


Projet ID3
==========

.. seealso::  http://id3svn/listing.php?repname=Projets.P2X238


Sur Framagit
=============

Le projet Django eurocoptergit add: https://framagit.org/pvergain/django_eurocopter


Exigences Eurocopter
====================

.. toctree::
   :maxdepth: 3

   exigences/index



Offre commerciale
=================

.. toctree::
   :maxdepth: 3

   offre_commerciale/index


Intervenants
============

.. toctree::
   :maxdepth: 3

   intervenants


Techniques
=================

.. toctree::
   :maxdepth: 3

   techniques/index

Courriels
=========

.. toctree::
   :maxdepth: 3

   courriels/index




Glossaire
============

.. toctree::
   :maxdepth: 3

   glossaire/index





