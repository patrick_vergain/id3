

.. index::
   pair: 20 juin 2012; Exigences



.. _exigences_20_juin_2012:
.. _ddld_10_241_indB:

=====================================================
Exigences 20 juin 2012 Eurocopter (DDLD-10-241-indB)
=====================================================


.. seealso::

   - :ref:`clause_a_clause`


.. contents::
   :depth: 3



Synthèse
========

Un projet d’implantation de puces RFID sur les pots de peinture, applicable dans
un premier temps aux peintures utilisées au bâtiment N1 (sur pièces mécaniques)
est en cours d’étude.

L’Up-Stream Spécification a été mise en place afin de réaliser le pré –screening
des puces RFID qui pourrait satisfaire aux exigences d’Eurocopter.

Les réponses à cet Up Stream Spécification seront données par les fabricants par
retour de courrier signé.

Ind B : ajout des exigences normes, fréquence, durée de vie de la puce et HSE



Champ d’information initiale (à remplir par fabriquant)
=======================================================


Informations initiales en écriture protégées:

- date de fabrication (8 alpha numériques maximum)
- nom (15 alpha numériques maximum)
- teinte (15 alpha numériques maximum)
- n°lot (30 alpha numériques maximum)
- n°ECS (6 alpha numériques maximum)
- date de péremption (8 alpha numériques maximum)
- T°C de stockage (Max 3 digits)
- Référence commerciale (20 alpha numériques maximum)
- Fournisseur (15 alpha numériques maximum)
- Quantité du pot (6 alpha numériques maximum)
- Donnée environnement:

    - Fiches de données de sécurité,
    - Base mono-pigmentaire (composition chimique ou FDS),
    - colorants de base

- Renseigner les références commerciales des constituants
  Base/Durcisseur/Diluant. Lors du scan, une alerte devra
  être émise si l’un des constituants n’est pas celui approprié.


Possibilité d’ajouter des données supplémentaires (champ libre
environ 6)


Champ d’information supplémentaire (à remplir par Eurocopter)
=============================================================

Des champs devront être disponibles par ajout automatique:

- Date de livraison du pot
- Date d’ouverture du pot

Il faudra pouvoir sélectionner lors du scan s’il s’agit d’une
ouverture de pot ou d’une réception produit suite à une livraison.

Des champs vides devront être disponibles afin qu’Eurocopter les
complète de façon manuelle :

- Possibilité d’indiquer la nouvelle date de péremption suite à
  l’ouverture du pot
- Possibilité d’indiquer si le lot de peinture a une revalidation
  LMP.

Ces champs seront en écriture protégée


HSE
====

.. versionadded:: révison B
   HSE.


Les matériaux et revêtements du tag devront être conformes à la réglementation
REACH et RoHS

Ils doivent être exempts de substances listées dans le document L085 002 ind D
(ECPDSL).


Type de tag
===========

Tag permettant d’être utilisé sur support métallique (sur partie cylindrique)



Support du tag
==============

Le Tag devra être sous la forme d’étiquette autoadhésive résistante à
l’arrachement.



Dimension du tag
================

Inférieure ou égale à 50 X 50mm


Épaisseur du tag
================

1 mm


Durée de vie du tag sur le pot
==============================

5 ans

Résistance aux agressions extérieures
=====================================

- Le tag devra être résistant aux chocs
- Le tag devra pouvoir évoluer dans un environnement métallique.
- Le tag devra pouvoir évoluer dans un milieu extérieur avec une
  hygrométrie élevée.
- Le tag pourra être confronté à des solvants ou des peintures

Tenue en température
====================

Le tag devra tenir entre -20°C à +75°C


Lecture
=======

Le tag pourra être lu n’importe où même lorsqu’il sera recouvert
d’une coulure de peinture


.. _distance_de_lecture:

Distance de lecture
===================

La lecture devra être possible à des distances supérieures à 50 mm.


.. _cadence_de_lecture:

Cadence de lecture
==================

.. versionadded:: révison B
   Cadence de lecture

Les tags devront pouvoir être lus un à un, manuellement par l’opérateur, dans un
délai de 30 secondes maximum.

Des lectures en masse pourront aussi être nécessaires (gestion des stocks)

.. _taux_de_lecture:

Taux de non lecture
===================

Le taux de non lecture devra être nul, chaque puce devra pouvoir être lu sans
difficulté.

Fréquence
=========

Fréquence UHF compatible dans le monde (pour environnement métallique)

Norme
=====

- EPC GEN 2
- ISO 18000-6
- ATA SPEC 2000 CHAP 9.5 (optionnel)


Certification
=============

Matériel devra être certifié Atex 3G



