

.. index::
   pair: Techniques; sqlite



.. _sqlite_Eurocopter:

=======================================
Sqlite Eurocopter
=======================================

.. seealso::

   - http://fr.wikipedia.org/wiki/SQLite


.. contents::
   :depth: 3


Introduction
============


SQLite est une bibliothèque écrite en C qui propose un moteur de base de données
relationnelle accessible par le langage SQL.

SQLite implémente en grande partie le standard SQL-92 et des propriétés ACID.

Contrairement aux serveurs de bases de données traditionnels, comme MySQL ou
PostgreSQL, sa particularité est de ne pas reproduire le schéma habituel
client-serveur mais d'être directement intégrée aux programmes.

L'intégralité de la base de données (déclarations, tables, index et données)
est stockée dans un fichier indépendant de la plateforme.


Exemples
========

Arduino RFID Authentication
---------------------------

.. seealso::

   - https://github.com/mattwilliamson/Arduino-RFID


This is an RFID authentication system. It allows a person to place an RFID tag
within proximity of the reader device (usually 5-6 inches with this reader) and
it will look up the tag in an SQLite database to check if it should grant access.

If authorization is granted, the hardware device will turn a servo motor into a
second position.

The primary application of this being the unlocking of a door.

This implementation also speaks aloud "Access Granted" or "Access Denied" if
you are running the server on OS X.

You can modify the source to use something like flite on other OSes.

This project is very easily modified to do any sort of task.


rfid.py
++++++++


.. literalinclude:: rfid.py
   :language: python
