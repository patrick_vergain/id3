



.. _eurocopter_solution_proposee:

=============================
Eurocopter Solution proposée
=============================


.. contents::
   :depth: 3

Introduction
============

La définition des besoins telle qu’énoncée dans le document de référence
:ref:`DDLD-10-241-indB <ddld_10_241_indB>` permet d’envisager différentes solutions présentant chacune ses
avantages et inconvénients.

Cependant, l’architecture matérielle et logicielle reste la même quelle que soit
la technologie:

- Lecteurs fixes et mobiles
- Tags fixés sur les pots de peinture
- Logiciels d’encodage et d’exploitation (lecture et mise à jour des événements
  dans le tag)
- Logiciel d’inventaire et de gestion de stock (option hors scope de cette offre)


.. _archi_materielle:

Architecture matérielle
=======================

Encodage des tags chez le fournisseur de peinture
--------------------------------------------------


Cette phase nécessite au minimum un lecteur de table afin d’encoder les tags.

Lecture et mise à jour des tags chez Eurocopter
-----------------------------------------------


Cette phase nécessite un lecteur mobile de type PDA permettant la lecture des
tags sur les pots de peinture, la saisie et l’écriture de données complémentaires
ou de mise à jour, la collecte et le stockage des informations lues (ou la
transmission via réseau) pour une exploitation future dans le cadre d’inventaire
ou de gestion de stock.

Un lecteur de table associé à un poste fixe au niveau du magasin de peinture
pourrait également être intéressant


.. _solution_logicielle:

Logiciel
=========

Logiciel d’encodage
-------------------

Le logiciel d’application sur PC permettra la saisie des informations préalables
et l’écriture dans les tags.

Il permettra également de stocker les informations ainsi codées dans une base
de données de type EXCEL.

Logiciel embarqué
-----------------

Le logiciel embarqué dans le Terminal mobile permettra la lecture et l’écriture
des tags, il permettra également de stocker les lectures et écriture dans un
fichier pour une exploitation sur poste fixe (gestion de stock ou inventaire),
il sera doté d’une interface homme-machine permettant une utilisation intuitive
pour l’opérateur.



