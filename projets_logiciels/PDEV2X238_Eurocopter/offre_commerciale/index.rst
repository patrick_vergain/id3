

.. index::
   pair: Offre commerciale; Eurocopter



.. _offfre_commerciale_Eurocopter:

=====================================
Offre commerciale faite par Orcanthus
=====================================

.. toctree::
   :maxdepth: 5


   2__besoins
   3__solution_proposee
   4__clause_a_clause/index
   5__lecteur_tablette
   6__terminal_mobile
   7__puces_rfid
   8__logiciel

