



.. _clause_a_clause:

=============================
Clause à clause (exigences)
=============================

.. seealso::

   - :ref:`exigences_20_juin_2012`


.. contents::
   :depth: 3


.. figure:: partie_1.png
   :align: center



.. figure:: partie_2.png
   :align: center




Remarque 1
==========

Ce tag permet de stocker 512 bits + 128 bits EPC (soit 80 caractères de 8 bits)
d’informations cela peut sembler insuffisant cependant il est possible
d’optimiser l’espace mémoire en utilisant des astuces de codage.

Par exemple:

- une date peut-être codée sur 3 x caractères Hexadécimaux au lieu de 8
  caractères ASCII,
- un nom de produit ou de fournisseur peut également être codé en caractère
  Hexa suivant une table de correspondance (Un caractère permet 256 possibilités)

Les informations sont bien contenues dans le tag mais sous forme codée, le moyen
de lecture possède le “décodeur” permettant de rendre intelligible l’information
pour l’opérateur.

Cette méthode permettrait de stocker la majorité des informations demandées.

Il est à noter qu’aucun tag ne permet le stockage de fichier pdf, en effet
ceux-ci nécessite une taille de mémoire de stockage très importante
(plusieurs KOctets par fichier)

Attention: quel que soit le tag choisi la taille mémoire n’est pas infinie =>
par exemple si un pot est ouvert et refermé 100 x fois dans son cycle de vie
cela représente au minimum 600 octets à stocker !


Remarque 2
==========

Ce tag ne se présente pas sous forme d’étiquette autocollante, cependant son
faible encombrement peut le rendre compatible avec ne utilisation sur support
courbe: Il ne mesure que 10,9 mm en largeur et 4, 8 mm de hauteur.
Un dispositif de protection peut également être prévu (strap ou étiquette)


Remarque 3
==========

Ce tag ne se présente pas sous forme d’étiquette autocollante, cependant il est
fixable par adhésif et résiste à l’arrachement

Remarque 4
==========

La conception des tags HF et UHF passif les rend potentiellement compatible avec
les normes ATEX, cependant il est nécessaire de prévoir un passage de tests en
laboratoire agréé pour obtenir la certification.

Cela peut entraîner des surcoûts au niveau des tags et (ou) un coût forfaitaire
de certification.

En fonction des tags choisis une évaluation chiffrée pourra être faite.



