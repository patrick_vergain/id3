

.. index::
   pair: Puces; RFID



.. _logiciel_eurocopter:

====================
Logiciel Eurocopter
====================

.. contents::
   :depth: 3


Logiciel d’exploitation - poste d’encodage
==========================================

Ce logiciel sera développé sur une base PC/Windows il aura les fonctionnalités
suivantes:

- Pilotage du lecteur RFID d’encodage (Création d’un middleware permettant la
  communication entre le lecteur Tablette et un PC via liaison USB)
- Procédure de création d’objet (encodage de l’étiquette radio et association
  avec un objet “pot de peinture”)
- Stockage des informations des produits étiquetés (sous Microsoft EXCEL)
- Interface opérateur (interface de dialogue permettant la configuration du système et
  l’exploitation des données)


Logiciel embarqué – Terminal mobile
===================================

Application Logiciel embarqué

- Application mobile sur PDA sous Windows Mobile
- Création d’une application permettant une lecture manuelle des étiquettes
  radio (TAG RFID) et l’intégration des données lues dans une base de données
  “Mobile” (fichier Exel ou compatible)
- Interface opérateur (IHM basique permettant à l’opérateur la saisie et
  l’enregistrement de données ( mode lecture ou mode écriture )
- Interfaçage base de données terminal mobile et base de données PC (option)




