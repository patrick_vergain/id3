

.. index::
   pair: Puces; RFID



.. _puces_rfid:

===========================
Puces RFID d’identification
===========================


.. contents::
   :depth: 3

Introduction
============


Il existe aujourd’hui une grande variété de tag HF & UHF et le choix est
fonction de plusieurs critères:

- Taille de l’objet
- Matière de l’objet (bois, métal, plastique....)
- Environnement de l’objet (agressions climatiques, chimiques....)
- Distance de détection souhaitée

Ces critères sont parfois contradictoires: il sera difficile par exemple, d’avoir
une grande distance de détection avec un tag de dimensions réduites adapté à
un petit objet

Pour l’application Eurocopter, les objets à identifier sont des pots de peinture
(cylindriques), en métal et évoluant dans un environnement difficile
(température, agressions chimiques, risque explosif).



.. _tags_HF:

Tags HF
=======

.. seealso::

   - :ref:`lecteur_HF`


Il s’agit d’un tag HF (13,56 Mhz) répondant aux normes 14 443A (MIFARE) conçu
pour fonctionner sur du métal.

Etant semi flexible il peut facilement se fixer par collage (adhésif sur sa
face arrière) sur des pièces cylindriques telles que des fûts, barils, tubes
ou pots.


Spécifications
--------------

- ISO/IEC 14443A ( MIFARE 1K) Mémoire utilisateur 1 KOctets (1024 octets)



.. _tags_UHF:

Tags UHF EPC
============

Ce tag UHF au format étiquette auto collante, fonctionne sur du métal.

Ses dimensions, sa forme et sa flexibilité lui permettent d’être parfaitement
adapté pour des pièces cylindriques (pots, fûts, tubes….).

Étant conforme aux standards EPC global UHF Gen2 et ISO 18000-6C, il peut être
utilisé partout dans le monde sur la bande 860~960 Mhz.


Spécifications
--------------

- EPC Class 1 Gen 2 (ISO 18000-6C) Mémoire utilisateur 512-bit



.. _tags_UHF_high_memory:

Tags UHF High Memory
====================

Ce tag fonctionnant sur métal est un des tags UHF durci, à mémoire étendue, les
plus petits du marché qui puisse répondre aux exigences de l’industrie
aéronautique (certifié ATA SPEC 2000 et SAE AS5678).


Spécifications
--------------

- EPC Class 1 Gen 2 (ISO 18000-6C) Mémoire utilisateur de 2k bits






