

.. index::
   pair: Terminal; Mobile



.. _terminaux_mobiles:

==================
Terminaux mobiles
==================


.. contents::
   :depth: 3


.. _terminal_mobile_standard_HF:

Terminal mobile standard (HF ou UHF en option)
==============================================


.. seealso:: http://www.orcanthus.com/fr/product_120/csat13.56868.htm


Lecteur RFID UHF portable, hautes performances, de grande autonomie et robuste.

Le lecteur CSAT13.56/868 offre les technologies de dernières générations dans un
ensemble intégré, modulaire, idéal pour les environnements de travail mobiles.


Le lecteur CSAT13.56/868 est un outil de terrain, qui offre la saisie manuelle
de données par un écran tactile (pop up du clavier ou du pavé numérique), ou
automatisée par un scanner 1D, 2D, HF 13.56 Mhz ou UHF 868 Mhz (au choix).

Avec une connectivité sans fil, répondant à la norme 802.11b/g 2.4 GHz, et large
zone, au travers des technologies CDMA et GSM/GPRS, le Lecteur CSAT13.56/868 se
comporte comme le satellite d’un système centralisé.

La liaison avec les applications du système d’informations central se fait en
direct ou en différé.

Grâce à une conception robuste, il est en mesure de travailler dans tous types
d’environnements, des plus faciles aux plus sévères.


Logiciels
---------

Microsoft® Win CE 5.0, Internet Explorer, Async, logiciel mise à jour Compact Flash, etc.

Développement applications
++++++++++++++++++++++++++

- Microsoft Embedded Visual C++®,
- Microsoft Visual Studio .NET®

Fréquence
----------

- HF (13.56MHz)
- UHF (US: 902-928MHz, EU: 865.6-867.6MHz, JP: 952-954MHz)


.. _terminal_mobile_ATEX_HF:

Terminal Mobile ATEX (HF ou UHF en option)
==========================================


Présentation
------------

Le terminal mobile TMC13.56/868-EX est un outil robuste et fiable pour la
collecte de données par lecture de tags RFID HF ou UHF (en fonction de l’option
choisie) en environnement ATEX.

Ces principaux atouts, outre son écran couleur tactile ¼ VGA et son environnement
Microsoft Pocket PC sont:

- UHF longue portée
- WIFI et Bluetooth intégré
- Batterie extractible et rechargeable
- Mémoire de masse évolutive (SD card)


OS
---

- Microsoft® Windows Mobile


Fréquence
---------

- HF (13.56MHz)
- UHF (EU: 865.6-867.6MHz


Type de tags
------------

- HF

  * ISO14443A/B
  * ISO15693

- UHF

  * EPC C1G2
  * ISO18000-6B/C










