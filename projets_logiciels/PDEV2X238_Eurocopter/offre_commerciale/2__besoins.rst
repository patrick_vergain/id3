



.. _besoins_eurocopter:

=============================
Besoins Eurocopter
=============================


.. contents::
   :depth: 3

Contexte
========

Le département “Green technologies” souhaite pouvoir suivre et tracer les
produits ou composants à risque intervenant dans la fabrication des hélicoptères.

Un projet d’implantation de puces RFID sur les pots de peinture, applicable dans
un premier temps aux peintures utilisées au bâtiment N1 (sur pièces mécaniques)
est en cours d’étude.


Les besoins
===========

Les besoins sont répertoriés dans un document Eurocopter de référence :ref:`DDLD-10-241-indB <ddld_10_241_indB>`.

La conformité des solutions proposées avec ce document sont décrite clause par clause (cf. Chapitre 4).
