

.. index::
   pair: Lecteurs; Tablette



.. _lecteurs_tablette:

=================
Lecteurs tablette
=================

.. contents::
   :depth: 3



.. _lecteur_HF_CL1356A:
.. _lecteur_HF:

Lecteur HF CL1356A
===================

.. seealso::

   - :ref:`tags_HF`



Présentation
------------

Le lecteur CL1356A est un lecteur sans contact (RFID) de bureau multi protocoles
capable de lire la plupart des cartes et transpondeurs (tags) du marché.

Il se distingue tout particulièrement des produits équivalents par sa rapidité,
sa capacité de discrimination, ses modes de fonctionnement, son ergonomie et sa
très grande fiabilité.

Ce lecteur de table fonctionne via une liaison USB.

Il fonctionne dans la bande de fréquence 13,56 MHz (HF)



.. _lecteur_UHF_NORSA868:

Lecteur UHF NORSA/868
=====================

.. seealso::

   - http://www.orcanthus.com/fr/product_121/norsa868.htm
   - :ref:`tags_UHF`


Présentation
------------

Le lecteur de table NORSA/868 a été conçu pour répondre aux besoins dans un
grand nombre d’applications:

- station RFID des points de ventes
- traçabilité des documents
- contrôle d’accès
- stations d’encodage
- kiosques multimédia...

La distance de lecture du lecteur NORSA/868 est paramétrable de quelques
centimètres à plusieurs mètres (en fonction du :ref:`tag <tags_UHF>` et de l’objet).

Nous le préconisons comme poste fixe d’encodage chez votre fournisseur, ou
comme poste de lecture fixe dans vos ateliers.



