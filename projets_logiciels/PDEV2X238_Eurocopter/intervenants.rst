

.. index::
   pair: Intervenants; Eurocopter

.. _intervenants_eurocopter:

========================
Intervenants Eurocopter
========================

.. contents::
   :depth: 3



Commercial
==========

.. seealso::

   - :ref:`marc_lavorel`


Chef de projet
==============

.. seealso::

   - :ref:`frederic_gautier`

Ingénieur logiciel
==================

.. seealso::

   - :ref:`patrick_vergain`


Contact Eurocopter
==================

.. seealso::

   - :ref:`aline_barbera`







