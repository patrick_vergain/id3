

.. index::
   pair: Base de données; sqlite

.. _courriel_26_octobre_2012:

=================================================================
Remarques sur le stockage des données (vendredi 26 octobre 2012)
=================================================================


.. seealso::

   - voir http://fr.wikipedia.org/wiki/SQLite


.. contents::
   :depth: 3

Entête courriel
===============

::

    Sujet:  Relecture de l'offre commerciale Eurocopter, stockage des informations
    Date :  Fri, 26 Oct 2012 15:21:04 +0200
    De :    Patrick VERGAIN <patrick.vergain@id3.eu>
    Organisation :  id3 Technologies
    Pour :  Marc LAVOREL <marc.lavorel@orcanthus.com>
    Copie: frederic.gautier@id3.eu



Stockage des données
====================

C'est juste une information par rapport au stockage de données + signalement
d'une inversion de lettres (voir tout en bas).

Une vraie base de données massivement utilisée par tout ce qui est mobile et
"petit" est la base de données sqlite.

Ce format est peut-être plus adapté que le format EXCEL ?


voir http://fr.wikipedia.org/wiki/SQLite

::

    Contrairement aux serveurs de bases de données traditionnels, comme MySQL
    ou PostgreSQL, sa particularité est de ne pas reproduire le schéma habituel
    client-serveur mais d'être directement intégrée aux programmes.

    L'intégralité de la base de données (déclarations, tables, index et données)
    est stockée dans un fichier indépendant de la plateforme.

    De par son extrême légèreté (moins de 300 Kio), elle est également très
    populaire sur les systèmes embarqués, notamment sur la plupart des smartphones
    modernes : l'iPhone ainsi que les systèmes d'exploitation mobiles Symbian
    et Android l'utilisent comme base de données embarquée. Au total, on peut
    dénombrer plus d'un milliard de copies connues et déclarées de la bibliothèque

    Adoption

    Outre son implantation officielle en C, des bindings pour d'autres langages
    existent (C++, Perl, Ruby, TCL, les langages utilisant le framework .NET
    via un pilote ADO.NET...).

    Quelques langages de programmation incluent SQLite dans leur bibliothèque
    standard, c'est le cas, entre autres, de Python (depuis la version 2.5) et
    de PHP (depuis la version 5).

    SQLite est utilisée dans de nombreux logiciels libres, comme Mozilla Firefox,
    dans de nombreuses distributions Gnu/Linux, dans les systèmes d'exploitation
    serveurs et de bureau comme Solaris ou mobiles comme Android ou Symbian,
    dans certains logiciels d'Apple, de Google, d'Adobe et de McAfee ainsi que
    dans certains appareils de Philips.



sqlite-manager
==============

Sous firefox, il existe une extension qui permet d'éditer facilement les données
d'une base sqlite.

Exemple:édition de la table "moz_items_annos" de firefox au moyen du gestionnaire
de données sqlite-manager.

.. figure:: sqlite.png
   :align: center

   Sqlite manager


sqlite manager


- http://code.google.com/p/sqlite-manager/wiki/FAQ



--
Patrick
