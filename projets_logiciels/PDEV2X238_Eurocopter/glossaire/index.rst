

.. index::
   pair: Glossaire; Eurocopter



.. _glossaire_Eurocopter:

=====================================
Glossaire
=====================================

.. glossary::


   EPC
       An Electronic Product Code, or EPC, is an electronic tag that contains a
       unique number. This number identifies one product from another.
       EPC is often called the next generation of the standard bar code.
       Like the bar code, EPC uses a numerical system to identify a product.

       An EPC is a number that can be associated with specific product information,
       such as date of manufacture, origin and destination of shipment.

       This information provides significant advantages for businesses and
       consumers. EPCs do not carry personally identifiable information.

       .. seealso:: http://www.gs1.org/aboutepc/faqs


   TID
       Transponder ID (TID) number, which identifies the chip type and any
       custom commands and optional features it supports, to authenticate the tag.

       .. seealso:: http://www.rfidjournal.com/article/view/4332


