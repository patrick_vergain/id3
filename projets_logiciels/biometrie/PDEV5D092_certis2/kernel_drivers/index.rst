

.. index::
   pair: Kernel Drivers; PDEV5D092



.. _PDEV5D092_kernel_drivers:

====================================================================
Certis2 fingerprint kernel drivers
====================================================================

.. contents::
   :depth: 3


Environnement logiciel
======================

L'ensemble des projets est écrit en langage C.


Documentation sphinx
====================

.. seealso::

   - svn+ssh://pvergain@svn/export/disk0/svn/P5D092/Documentation/certis_drivers


Dépot subversion
================


- svn+ssh://pvergain@svn/export/disk0/svn/P5D092/Concept/Soft/PC/Driver

