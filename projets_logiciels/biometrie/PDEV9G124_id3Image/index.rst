

.. index::
   pair: Projets; PDEV9G124
   pair: Projets; id3Image
   pair: Projets; C


.. _id3_image:

================================
PDEV9G124, bibliothèque id3Image
================================


.. seealso::

   - file:///C:/projects_id3/P9G124/XLOG9Y347_docId3Image/doc_sphinx/build/html/index.html


.. contents::
   :depth: 3
   

Environnement logiciel
======================

L'ensemble des projets est écrit en langage C.



Dépot subversion
================

- :file:`/export/disk0/svn/P9G124/Concept/Soft/PC/XLOG9Y315_LIB_id3Image`



Documentation id3Image
=======================

.. seealso::

   - file:///E:/users/pvergain/id3_projects/doc_id3Image/build/html/index.html


Dépôt subversion
----------------

- :file:`/export/disk0/svn/P9G124/Documents/id3/XLOG9Y347_docId3Image`

