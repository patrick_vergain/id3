
.. index::
   ! Projets biometrie

.. _projets_biometrie:

=================
Projets biométrie
=================

.. toctree::
   :maxdepth: 3



2014
=====

.. toctree::
   :maxdepth: 3


   PDEV0Q075_id3fingerToolkit/index
   PDEV4N307_TestCapteurs/PDEV4N307_TestCapteurs
   PDEV3N161_id3_BioAPI/index
   

2010-2013
=========
    
.. toctree::
   :maxdepth: 3
       
   PDEV5D092_certis2/index
    

2010-2011
=========
    
.. toctree::
   :maxdepth: 3

   PDEV9G124_id3Image/index

 
  

   
   
