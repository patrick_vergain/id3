

.. index::
   pair: Projets; C++
   pair: Projets C++;  Codes Rousseau
   pair: Projets C++;  XLOG1X805


.. _projet_XLOG1X805_codes_rousseau:

=====================================
Projet C++ XLOG1X805 Codes Rousseau
=====================================


.. seealso::

   - https://svn.srv.int.id3.eu/


.. contents::
   :depth: 3


Environnement logiciel
======================

- Le projet est écrit en C++ (Qt4)
- L'environnement de développement utilisé est `Qt Creator`_ .
- `Qt SDK`_

.. seealso::

   - http://pvdevtools.readthedocs.org/en/latest/devel/IDEs/Qt/sdk/index.html
   - http://pvdevtools.readthedocs.org/en/latest/devel/IDEs/Qt/creator/versions/index.html


.. _`Qt SDK`: http://pvdevtools.readthedocs.org/en/latest/devel/IDEs/Qt/sdk/index.html
.. _`Qt Creator`: http://pvdevtools.readthedocs.org/en/latest/devel/IDEs/Qt/creator/versions/index.html



Documentation sphinx
=====================

.. seealso::

   - file:/C:/projects_id3/XLOG8X020_CL1356T_id3_Standard/3.3.4/docs/sphinx/build/html/index.html


.. livraison_cr_easytest_evolution:

Livraison ``cr_easytest_evolution``
===================================

Le fichier zippé est nommé :file:`EasytestEvolution_LibrairieAutoEcole.r$numero_subversion$`



Dépot subversion  XLOG1X805
===========================

.. seealso::

   - svn+ssh://pvergain@svn/export/disk0/svn/P1V160/Concept/Soft/PC/XLOG1X805_CR_Euclide



.. _bitbucket_cplus_plus_codes_rousseau:

Bitbucket C++ Codes Rousseau
=============================

.. seealso::

   - https://bitbucket.org/pvergain/codes_rousseau
