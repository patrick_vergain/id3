
.. index::
   pair: Projets python ; id3 Semiconductors
   pair: Projets python;  Codes Rousseau
   pair: Projets python;  XLOG1Y043



.. _projet_python_codes_rousseau:

===================================================
Projet python XLOG1Y043 Codes Rousseau
===================================================


.. contents::
   :depth: 3


Environnement logiciel
======================

- L'environnement de développement utilisé est Visual studio 2012 ou pyScripter.



Dépot subversion
================

- :file:`/export/disk0/svn/P1V160/Concept/Soft/PC/Python/XLOG1Y043_Easytest2PythonModule`
- svn+ssh://pvergain@svn/export/disk0/svn/P1V160



.. _bitbucket_python_codes_rousseau:

Bitbucket python Codes Rousseau
===============================

.. seealso::

   - https://bitbucket.org/pvergain/py_codes_rousseau



