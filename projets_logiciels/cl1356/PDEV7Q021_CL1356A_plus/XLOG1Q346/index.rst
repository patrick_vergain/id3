
.. index::
   pair: Projets;  CL1356A+
   pair: Projets C++;  CL1356A+
   pair: Projets C++;  XLOG1Q346


.. _projet_XLOG1Q346_cl1356a_plus:

=======================
Projet C++ XLOG1Q346
=======================


.. seealso::

   - http://id3svn/listing.php?repname=Projets.P7Q021&path=%2FConcept%2FSoft%2FPC%2FXLOG1Q346_CL1356A_plus%2F&#a0396c23a5c061fc631f2ff813b0ec89f

.. contents::
   :depth: 3


Environnement logiciel
======================

- Le projet est écrit en C++ (Qt)
- L'environnement de développement utilisé est Qt Creator.


Liens
=====

Sur le bureau
-------------

- :file:`Y:\XLOG1Q346_CL1356A_plus`



Dépot subversion
================

- :file:`/export/disk0/svn/P7Q021/Concept/Soft/PC/XLOG1Q346_CL1356A_plus`
- svn+ssh://pvergain@svn/export/disk0/svn/P7Q021


.. _bitbucket_cplus_plus_cl1356a_plus:

Bitbucket C++ CL1356A+
=======================

.. seealso::

   - https://bitbucket.org/pvergain/cl1356a_plus_ng
   - https://bitbucket.org/pvergain/cl1356a_plus






