
.. index::
   pair: XLOG1Q318 ; versions



.. _XLOG1Q318_versions:

===================================================
XLOG1X318 (CL1356APlusPythonPackage) releases
===================================================


.. contents::
   :depth: 3


v1.40
=====

Package CL1356A
    Adds MIFARE, ISO15693 and Firmware update examples.
    Replaces "from x import \*" statement by "import x" statement

Package Enum
    Compare enums based on their name instead of their values.
    This allows enum members with same value but different names.

v1.30
=====

Package CL1356A
    Adds ISO15693 management module.
    Adds reader firmware update commmands.

v1.20
=====

Package CL1356A
    Adds MIFARE access conditions management module.
    Adds MIFARE Increment/Decrement/Transfert/Restore commmands.

v1.10
=====

Package CL1356
    Adds GetReadersStatus command.
    Adds GetCardStatus command.

Package CL1356A
    Adds HID enable/disable command.
    Adds MIFARE commands.

v1.00
=====

First release




