
.. index::
   pair: Projets python ; id3 Semiconductors
   pair: Projets python;  CL1356A+
   pair: Projets python;  XLOG1Q318



.. _projet_python_cl1356a_plus:

===================================================
Projet python  XLOG1X318 (CL1356APlusPythonPackage)
===================================================


.. contents::
   :depth: 3


Environnement logiciel
======================

- L'environnement de développement utilisé est Visual studio 2012 ou pyScripter.



Dépot subversion
================

- :file:`/export/disk0/svn/P7Q021/Concept/Soft/PC/XLOG1X318_CL1356APlusPythonPackage`
- svn+ssh://pvergain@svn/export/disk0/svn/P7Q021


- :file:`/export/disk0/svn/P7Q021/Concept/Soft/PC/XLOG1X318_CL1356APlusPythonPackage/v1_00`



.. _bitbucket_python_cl1356a_plus:

Bitbucket python CL1356A+
=========================

.. seealso::

   - https://bitbucket.org/pvergain/py_cl1356a_plus


Versions
========

.. toctree::
   :maxdepth: 3

   versions/index

