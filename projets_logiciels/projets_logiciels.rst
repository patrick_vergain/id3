
.. index::
   ! Projets logiciels
   pair:  Projets logiciels; id3
   pair: Déroulement; Projet



.. _projets_logiciels_id3:

======================
Projets Logiciels id3
======================


.. seealso::

   - https://svn.srv.int.id3.eu/

.. contents::
   :depth: 3



2016
======

Janvier 2016
-------------

Travail sur le CL1356A. Le but est de fournir des exemples de code
"application".


2014
=====


Biométrie
---------

.. toctree::
   :maxdepth: 3

   biometrie/PDEV3N161_id3_BioAPI/index
   biometrie/PDEV4N307_TestCapteurs/PDEV4N307_TestCapteurs

Colombie
---------

.. toctree::
   :maxdepth: 3

   PDEV2V330_colombie/PDEV2V330_colombie


Reprise Jérôme 2014
--------------------

.. toctree::
   :maxdepth: 3

   reprise_jerome/reprise_jerome
   site_web_id3/site_web_id3


2013-2014
===========

.. toctree::
   :maxdepth: 3


   cea/PDEV3Y071_CEA_Wavelens/PDEV3Y071_CEA_Wavelens

2012-2014
==========

.. toctree::
   :maxdepth: 3

   PDEV2X238_Eurocopter/PDEV2X238_Eurocopter
   cl1356/PDEV1V160_EasyTest2/index

2008-2016
=========

.. toctree::
   :maxdepth: 3

   cl1356/PDEV7Q021_CL1356A_standard/PDEV7Q021_CL1356A_standard


2011-2014
==========

.. toctree::
   :maxdepth: 3

   cea/PDEV1S173_CEA_Imageur/index


2011-2014
=========

.. toctree::
   :maxdepth: 3

   cl1356/PDEV7Q021_CL1356A_plus/index


2010-2013
=========

.. toctree::
   :maxdepth: 3

   biometrie/PDEV5D092_certis2/index


2011-2012
=========

.. toctree::
   :maxdepth: 3

   cea/PDEV0W309_CEA_RYB/index

2008-2012
=========

.. toctree::
   :maxdepth: 3

   cl1356/PDEV5J577_CL1356T/index

2010-2011
=========

.. toctree::
   :maxdepth: 3

   biometrie/PDEV9G124_id3Image/index


2009
=========

.. toctree::
   :maxdepth: 3


   cl1356/PDEV5E627_Easytest1/index




Other projects
==============

.. toctree::
   :maxdepth: 3

   cl1356/PDEV5J577_CCID_Driver/index
   cl1356/winscard_win32/index
   bitbucket


Projets biométrie
=================

.. toctree::
   :maxdepth: 3

   biometrie/biometrie


Projets CEA
=================

.. toctree::
   :maxdepth: 3

   cea/cea


Projets CL1356
=================

.. toctree::
   :maxdepth: 3

   cl1356/cl1356


Gestion des projets
===================

.. toctree::
   :maxdepth: 3

   gestion_des_projets/gestion_des_projets
