
.. index::
   pair: Projets logiciels; bitbucket
   pair: bitbucket; pvergain


.. _projets_bitbucket:

======================
Projets bitbucket
======================


.. seealso::

   - https://bitbucket.org/pvergain/id3_work
   - https://bitbucket.org/pvergain/


CL1356A plus
============

.. seealso::

   - https://bitbucket.org/pvergain/cl1356a_plus
   - https://bitbucket.org/pvergain/cl1356a_plus_ng


- :ref:`bitbucket_cplus_plus_cl1356a_plus`
- :ref:`bitbucket_python_cl1356a_plus`



winscard_win32
===============

.. seealso::   https://bitbucket.org/pvergain/winscard_win32


Codes Rousseau
===============


