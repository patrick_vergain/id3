
.. index::
   pair: 2012 ; OVH

.. _ovh_2012:

==============================
VPS OVH (21 novembre 2012)
==============================


::

    Sujet: 	[vps14482.ovh.net] Installation de votre VPS
    Date : 	Wed, 21 Nov 2012 08:35:19 +0100 (CET)
    De : 	Support OVH <support@ovh.com>
    Pour : 	pierre.marc@id3.eu


SAS OVH - http://www.ovh.com
2 rue Kellermann
BP 80157
59100 Roubaix


Bonjour,

Votre VPS vient d'être installé sous le système d'exploitation
Windows 2008 Server Web Edition 
(en version 64 bits)


PARAMETRES D'ACCES
===================

L'adresse IP du VPS est : 46.105.12.14

Le nom du VPS est : vps14482.ovh.net

(jusqu'à 48h peuvent être nécessaires pour que ce nom soit actif)

Le compte administrateur suivant a été configuré sur le VPS :
(accès au serveur via Terminal Server ou SSH)

- Nom d'utilisateur : root
- Mot de passe : qRSLxsrA

Enfin, vous pourrez gérer votre VPS, ajouter ou modifier des options
depuis votre manager à l'adresse suivante :
https://www.ovh.com/managerv5


OBTENIR DE L'AIDE
==================

Pour vous accompagner dans la prise en main de votre VPS, nous
mettons à votre disposition de nombreux guides d'utilisation :
http://guides.ovh.com

D'autre part, une importante communauté d'utilisateurs est
accessible via notre forum et nos mailing-listes :
http://www.ovh.com/fr//fr/support/help/communaute.xml

Enfin, si vous recontrez un dysfonctionnement de votre VPS,
vous pouvez déclarer un incident 24h/24 et 7j/7 :
http://www.ovh.com/fr//fr/support/declarer_incident.xml


Nous espérons que votre VPS vous apportera entière satisfaction
et nous restons à votre disposition pour tout complément d'information.


Cordialement,

Support Client OVH
Support Technique : 08.99.49.87.65 ( 1.349 Euro/appel + 0.337 Euro/min)
Support Commercial : 08.20.69.87.65 (Numéro Indigo 0.118 Euro/min)
Fax : 03.20.20.09.58
Email: support@ovh.com
Du lundi au vendredi : 8h - 20h
Le Samedi :            9h - 17h


