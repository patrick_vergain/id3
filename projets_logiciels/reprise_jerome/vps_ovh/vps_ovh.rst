
.. index::
   pair: Projet ; OVH
   ! Serveur dédié virtuel

.. _ovh:

==============================
Serveur dédié virtuel OVH
==============================



.. seealso::

   - http://www.wikiwand.com/fr/Serveur_d%C3%A9di%C3%A9_virtuel
   
 
.. contents::
   :depth: 3  
  
Introduction
============

   
Un serveur dédié virtuel (également appelé serveur virtuel), en anglais virtual 
private server (VPS) ou virtual dedicated server (VDS) est une méthode de 
partitionnement d'un serveur en plusieurs serveurs virtuels indépendants qui 
ont chacun les caractéristiques d'un serveur dédié, en utilisant des techniques 
de virtualisation. 

Chaque serveur peut fonctionner avec un système d'exploitation différent et 
redémarrer indépendamment. 

Dans le domaine de l'hébergement web, plusieurs dénominations recoupent le même 
type d'offres et donc de services. 

Les acronymes VPS (Virtual Private Server) et VDS (Virtual Dedicated Server) 
désignent le même concept, et leur usage est parfois confus.


Historique
==========


.. toctree::
   :maxdepth: 3
   
   1__21_novembre_2012
