.. index::
   pair: Téléphone; 323 Patrick VERGAIN
   pair: OpenPGP ; 0xB45AD1F6
   pair: Patrick ; VERGAIN


.. _patrick_vergain:

==================================================================
Patrick VERGAIN (323, Identifiant de clé OpenPGP: B45AD1F6)
==================================================================


- http://www.id3.eu

Adresse id3 Technologies
==========================

::

    5 rue de la Verrerie
    38120 Fontanil-Cornillon
    Rhône Alpes -

    Téléphone : +33 (0) 4 76 75 75 85
    Télécopie : +33 (0) 4 76 75 52 30
    Site web : http://www.id3.eu



.. _qualification:

Qualification, Convention collective
====================================

:Niveau: II
:coefficient: 108

Forfait 218 jours.



.. toctree::
   :maxdepth: 3

   convention_collective/index


Les valeurs
============

.. toctree::
   :maxdepth: 3

   les_valeurs/index


OpenPGP clé publique (Identifiant de clé : B45AD1F6)
=====================================================

.. seealso::

   - http://www.python.org/download/#pubkeys


:Identifiant de clé: B45AD1F6
:Empreinte: 7DF3 26DE 3F6B 0F1C 301A 35A3 0901 81D4 B45A D1F6


::

    -----BEGIN PGP PUBLIC KEY BLOCK-----
    Version: GnuPG v2.0.20 (MingW32)

    mQENBFLOje0BCADieauMbOpaWzWbtyx1n3bVJrMKGtI5WC8xCAGjel4fNSXE5rri
    SEt0t+bkCkWXEl7cW2YlSZF3ZSKEy6vMkiVQtfCey9kcT56YGjfKSbqhZmosKYtk
    kujZYCFnBVEF7jDb3mac/NPjeZQlJG3pfJl/frytgJKcxY5vSKbEnB+0ecbpChFD
    0QimAIX6S70vW8NW97903PtHJSra9Rn2lsZQ4sPYLSn8LGmzSKoaDiRm8kho9Kh/
    RPwc+V5GwvhznDb9mle7aNV2jpzL5mR5xlA45Vbk+fK83khU6SxRvrI5wh27STGr
    UuGU5Zgo8amWOWkVcMMpsfzVo96LEnVekooRABEBAAG0O1BhdHJpY2sgVmVyZ2Fp
    biAoaWQzIFRlY2hub2xvZ2llcykgPHBhdHJpY2sudmVyZ2FpbkBpZDMuZXU+iQE4
    BBMBAgAiBQJSzo3tAhsDBgsJCAcDAgYVCAIJCgsEFgIDAQIeAQIXgAAKCRAJAYHU
    tFrR9sKdCADWBV4uAMq6dYFaOebFP8H/fk2/1q57af75+6m+kdyRN7KEzkImeGPS
    Z4vgUzwL5YJzjI6W7gMBu4X0TxDqvllsnMV+c+DSgvFvgVpMGURFuh1U+Im55tua
    oxlEHoNWldbhlsHz6FSkmCxcVVNnuwACI1wwhZOGJtfZi2Fdnd/E99uVwPlZLT2l
    2VzAgmy3S9BZqlTghAybvOPVh5r0D9FHeR+xDq557OJW7mdqTv1cpdcZS2voH7yH
    NHVUrzHz8vMp9Zq6Fry+8Y31gTey44f2KChUPl7KlbO8813qhIKte7Lzb4xhfrxx
    ZNovByGPQjL02FGO0EQwkl7HNlo2JiJCuQENBFLOje0BCAC0tXOLgMIcO5/R314b
    8ICXbb7ZmIZoKXl41n/VyeR6yGqJES9wgg/DpYtsGx9dHx8AOWl3e4jnv8ZB+q8i
    JvkVUNZzeQCOejl1GR2t+m3yei6dwgX0QOBZ00QGxgL3iJ8wWlNxmkpqayVeS2Qv
    uU91bV8j8LA1wjfx70LVb1zG5Ba83uWSqRfDA90BpUG6kfCwh2PIjqVcj5PffHw3
    vfMSCSwdsDoTuo4Kz+aNn/85L2JNmzEeI8znf0i91NnAgavHKvI7l/KGMey0IEOw
    xDU5mGZoRHE0rzYBcOZRwKPjjsvlhLhYOjy1daN3y1QoU5smyILk5NOLYmb2F+Za
    3TgdABEBAAGJAR8EGAECAAkFAlLOje0CGwwACgkQCQGB1LRa0faMPAf/ZLeB/FEX
    LHMP2VcPJFfMknCIpVQk5C/AE7RR1zVbaodZR721IMNZxYsd8wDWmCPXi/MVvKLX
    hSlW6k+E3t7IVpdbX5yYRUt5bJtvre6z72/jLyTGNzXWtRwLS40aMn7n+A7F2vvZ
    EupA6KlpjnYDo3pj6isbUAOqqxGigqYf8Ek+KacPNvqvJi53XtHp7lgqxFaHFnHr
    ZeHRwX+8DmSVmMrpjk1Xt1zLCxS8rA7NVZglwtjOUTCnEiTXzbitCm1FunmYtt1m
    cyaDrC/BPlcauiauGW/bephty8jLe7NhQ+6dFRZwJX8UGpH8na8H/CkEvFMZfOf5
    RLFrzwtvLIdSsw==
    =Sakn
    -----END PGP PUBLIC KEY BLOCK-----


Gestion d'entreprises
======================

.. toctree::
   :maxdepth: 3

   gestion_entreprise/index


Actions/News
=============

.. toctree::
   :maxdepth: 3

   actions_news/actions_news
   courriels/courriels

Outils id3
=================

.. toctree::
   :maxdepth: 6

   outils_id3/outils_id3
   bookmarks/bookmarks


Projets Logiciels
=====================

.. toctree::
   :maxdepth: 6

   projets_logiciels/projets_logiciels


Réalités
========


.. toctree::
   :maxdepth: 3

   realites/index


Séminaires
=================

.. toctree::
   :maxdepth: 3

   seminaires/index


Glossaire
=========


.. toctree::
   :maxdepth: 3

   glossaire/index
