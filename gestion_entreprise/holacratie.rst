

.. index::
   ! Holacratie

   
.. _holacratie:

=======================================
Holacratie
=======================================

.. seealso::

   - http://www.cadreo.com/actualites/dt-l-holacratie-un-modele-d-entreprise-sans-manage


Supprimer tous les postes de managers et passer à un modèle d'organisation hiérarchique égalitaire, c'est la révolution du management promise par le système de l'holacratie. Mais d'où vient ce nouveau concept ?

Régulièrement de nouvelles modes de management émergent. Ces derniers temps on entend beaucoup parler de l'holacratie. Le terme n'est pas nouveau, il s'inspire du concept d'Holarchie décrit en 1967 par Arthur Koestler dans son livre "The Ghost in the Machine". Mais ce n'est qu'en 2007 que Ternary Software, un éditeur de logiciels américain, le formalise en tant que mode d'organisation après plusieurs années de réflexion. Transposé dans le monde de l'entreprise, ce système peut se résumer à de l'autorité distribuée au sein de cercles qui fonctionnent de manière autonome. Ce n'est plus le dirigeant en haut de la pyramide qui décide de tout, chaque collaborateur jouant plusieurs rôles en fonction de ses compétences. Avec l'holacratie, les chefs disparaissent, l'organigramme est remplacé par des cellules indépendantes, comme dans le corps humain.
Les intitulés de postes remplacés par des missions

Ce mode d'organisation semble idéal sur le papier, mais pour le mettre en place, il faut dépasser les résistances culturelles et remettre en cause tous les principes managériaux classiques. L'entreprise Zappos, un site de e-commerce a fait récemment cette révolution. Les 1500 salariés se sentent désormais responsables de la réussite de l'entreprise, les managers ont disparu, il n'y a plus d'intitulés de postes, chacun gère ses missions en fonctions d'objectifs précis. Pour l'instant, l'entreprise californienne est en phase d'expérimentation de l'holacratie jusqu'à la fin de l'année. Si ça marche elle pourrait bien en inspirer d'autres. Car pour le moment, peu d'entreprises ont adopté ce modèle. La société IGI Partners, qui accompagne les entreprises vers l'holacratie, estime qu'une quarantaine de sociétés aux Etats-Unis, mais aussi en France et en Belgique fonctionnent de la sorte.

Finalement l'holacratie, n'est rien d'autre qu'un système d'exploitation destiné à favoriser l'engagement des salariés et encourager l'efficacité. L'holacratie est sans doute plus facile à mettre en place dans des petites structures comme des start-up, mais les grands groupes ou les entreprises qui ont connu une croissance rapide, devraient s'y intéresser pour favoriser les innovations internes et trouver un mode de fonctionnement plus " agile "


