

.. index::
   ! Surveillance


   
.. _surveillance:

=======================================
Surveillance
=======================================

.. seealso::

   - http://www.internetactu.net/2014/06/04/la-demesure-est-elle-le-seul-moyen-pour-changer-doutil-de-mesure/
   - http://www.internetactu.net/2013/05/03/lemploi-a-lepreuve-des-algorithmes/
