

.. index::
   pair: Gestion; Entreprise


   
.. _gestion_dentreprise:

=======================================
Gestion d'entreprise
=======================================


.. toctree::
   :maxdepth: 3
   
   holacratie
   surveillance
