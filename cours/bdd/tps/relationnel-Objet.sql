drop table Operation;

drop table CptEpargne;
drop table CptCourant;

drop table Client;
drop table Mouvement;
drop table Compte;

drop type T_liste_telephone force;
/
drop type T_Compte force;
/
drop type T_CptCourant force;
/
drop type T_CptEpargne force;
/
drop type T_estSignataire force;
/
drop type T_appartientA force;
/


-- Telephone
Create type T_liste_telephone as VARRAY(3) of NUMBER(10);
/

-- Compte
Create type T_Compte as object (
	nCompte int,
	solde int,
	dateOuv date
) not final;
/

--possedePar int foreign key references Client(nClient)


/*Create table Compte(
	nCompte int primary key,
	solde int,
	dateOuv date,
	possedePar int foreign key references Client(nClient)
);
/*/

-- CptEpargne
Create type T_CptEpargne under T_Compte (
	taux real
);
/

/*Create table CptEpargne (
	nCompte int primary key,
	solde int,
	dateOuv date,
	possedePar int foreign key references Client(nClient),
	taux real
);*/




-- Predeclaration
Create type T_CptCourant;
Create type T_estSignataire;
Create type NT_estSignataire;
Create type T_appartientA;
Create type NT_appartientA;

-- CptCourant
/*Create table CptCourant(
	nCompte int primary key,
	solde int,
	dateOuv date,
	possedePar int foreign key references Client(nClient),
	nbMouvements int,
	appartientA NT_appartientA
)*/
Create type T_CptCourant under T_Compte (
	nbMouvements int,
	appartientA NT_appartientA
);

Create table CptCourant of T_CptCourant (
	primary key(nCompte)
)NESTED TABLE appartientA STORE AS TousSignataire;
--/


-- Client
/*Create table Client(
	nClient int primary key,
	nom varchar(40),
	adresse varchar(255),
	email varchar(100),
	telephones T_liste_telephone,
	estSignataire NT_estSignataire
)
NESTED TABLE estSignataire STORE AS TousCCP;
/*/

Create table Client(
	nClient int primary key,
	nom varchar(40),
	adresse varchar(255),
	email varchar(100),
	telephones T_liste_telephone,
	estProprietaire T_Compte,
	estSignataire NT_estSignataire
)
NESTED TABLE estSignataire STORE AS TousCCP;
/

-- estSignataire
Create type T_estSignataire as object (
	estSignataire T_CptCourant,
	droit int
);
--/

Create type NT_estSignataire as table of T_estSignataire;
--/

Create type T_appartientA as object (
	nclient int foreign key references Client(nclient),
	droit int
);
--/

Create type NT_appartientA as table of T_appartientA;
--/


-- Mouvement
Create table Mouvement(
	dateMouv date primary key
);
--/

Create table Operation(
	nClient int foreign key references Client(nClient),
	nCompte int foreign key references CptCourant(nCompte),
	dateMouv date foreign key references Mouvement(dateMouv),
	montant int,
	primary key(nClient, nCompte, dateMouv)
);