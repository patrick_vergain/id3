select * from emp;
select * from dept;
select * from salgrade;

-- Question 1 :
select empno, ename from emp;

-- Question 2 :
select empno, ename, hiredate 
from emp 
where deptno = 20;

select empno, ename, hiredate from emp where deptno = 20;

-- Question 3 :
select sum(sal) from emp;

-- Question 4 :
select e.empno, e.ename, e.job, e.sal from emp e where e.sal > 2800 and job = 'MANAGER';

select e.empno, e.ename, e.job, e.sal 
from emp e 
where e.sal > 2800 and job = 'MANAGER';

-- Question 5 :
select e.empno, e.ename 
from emp e
join dept d on e.deptno = d.deptno
where loc = 'DALLAS';

select e.empno, e.ename from emp e join dept d on e.deptno = d.deptno where loc = 'DALLAS';

-- Question 6 :
select e.empno, e.ename, e.job, e.sal
from emp e
where e.deptno = 30
order by e.sal asc;

select e.empno, e.ename, e.job, e.sal from emp e where e.deptno = 30 order by e.sal asc;

-- Question 7 :
select distinct(job) from emp;

-- Question 8 :
select empno, ename
from emp
where ename like 'M%' and sal >= 1290;

select empno, ename from emp where ename like 'M%' and sal >= 1290;

-- Question 9 :
select loc
from dept d
join emp e on e.deptno = d.deptno
where ename = 'ALLEN';

select loc from dept d join emp e on e.deptno = d.deptno where ename = 'ALLEN';

-- Question 10 :
select distinct(d.deptno), d.dname
from dept d
join emp e on e.deptno = d.deptno
where job = 'CLERK' or job = 'SALESMAN' or job = 'ANALYST';

select distinct(d.deptno), d.dname from dept d join emp e on e.deptno = d.deptno where job = 'CLERK' or job = 'SALESMAN' or job = 'ANALYST';

-- Question 11 :
select distinct(e2.empno), e2.ename 
from emp e1 
join emp e2 on e1.mgr = e2.empno 
where e2.job = 'MANAGER' and e1.comm is not null and e1.comm > 0;

select distinct(e2.empno), e2.ename from emp e1 join emp e2 on e1.mgr = e2.empno where e2.job = 'MANAGER' and e1.comm is not null and e1.comm > 0;

-- Question 12 :
select e.empno, e.ename
from emp e 
join dept d on e.deptno = d.deptno
where (d.loc = 'DALLAS' or d.loc = 'CHICAGO') and sal > 1000;

select e.empno, e.ename from emp e join dept d on e.deptno = d.deptno where (d.loc = 'DALLAS' or d.loc = 'CHICAGO') and sal > 1000;

-- Question 13 :
select distinct(d.deptno), d.dname, e.job, sum(e.sal) as sumSal, count(e.empno) as nbEmp, avg(e.sal) as avgSal  
from dept d 
join emp e on e.deptno = d.deptno
group by d.deptno, d.dname, e.job
having count(e.empno) > 2
order by d.deptno;

select distinct(d.deptno), d.dname, e.job, sum(e.sal) as sumSal, count(e.empno) as nbEmp, avg(e.sal) as avgSal from dept d join emp e on e.deptno = d.deptno group by d.deptno, d.dname, e.job having count(e.empno) > 2 order by d.deptno;

-- Question 14 :
select e.empno, e.ename, e.sal, s.grade, e2.ename, e.deptno, d.loc
from salgrade s, emp e 
left join emp e2 on e.mgr = e2.empno 
join dept d on d.deptno = e.deptno
where e.sal >= s.losal and e.sal =< s.hisal
order by e.sal;

select e.empno, e.ename, e.sal, s.grade, e2.ename, e.deptno, d.loc from salgrade s, emp e left join emp e2 on e.mgr = e2.empno join dept d on d.deptno = e.deptno where e.sal >= s.losal and e.sal <= s.hisal order by e.sal;

-- Question 15 :
select e.deptno, avg(e.sal) as avgSal
from emp e 
group by e.deptno
union
select deptno, 0 as avgSal
from dept
where deptno not in (select deptno from emp);

select e.deptno, avg(e.sal) as avgSal from emp e group by e.deptno union select deptno, 0 as avgSal from dept where deptno not in (select deptno from emp);

-- Question 16 :
select e.empno, e.ename, (e.comm / (e.comm+e.sal) * 100) as propComm, (e.sal / (e.comm+e.sal) * 100) as propSal
from emp e 
where e.comm is not null
union 
select e.empno, e.ename, 0 as propComm, 100 as propSal
from emp e 
where e.comm is null;


select e.empno, e.ename, (e.comm / (e.comm+e.sal) * 100) as propComm, (e.sal / (e.comm+e.sal) * 100) as propSal from emp e where e.comm is not null union select e.empno, e.ename, 0 as propComm, 100 as propSal from emp e where e.comm is null;

-- Question 17 :

select e.deptno, sum(e.sal)
from emp e 
group by e.deptno
having sum(e.sal) = (select max(sum(sal)) from emp group by deptno) 
or sum(e.sal) = (select min(sum(sal)) from emp group by deptno);

select e.deptno, sum(e.sal) from emp e group by e.deptno having sum(e.sal) = (select max(sum(sal)) from emp group by deptno) or sum(e.sal) = (select min(sum(sal)) from emp group by deptno);

-- Question 18 :
select d.deptno 
from dept d 
join emp e on e.deptno = d.deptno 
where d.loc != 'CHICAGO'
group by d.deptno
having count(e.empno) >= 3;

select d.deptno from dept d join emp e on e.deptno = d.deptno where d.loc != 'CHICAGO' group by d.deptno having count(e.empno) >= 3;

-- Question 19 :
select deptno from dept where deptno not in (select deptno from emp);

-- Question 20 :
select deptno
from emp
group by deptno
having count(empno) = (select max(count(empno)) from emp group by deptno);

select deptno, count(empno) from emp group by deptno having count(empno) = (select max(count(empno)) from emp group by deptno);

