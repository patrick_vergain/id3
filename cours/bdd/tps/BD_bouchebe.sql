begin;

-- Milieu(typeAtmo {pk})
CREATE TABLE Milieu(
	typeAtmo AS ENUM ('Acide', 'Basique', 'Neutre')
);

-- E_TypeM(typeM {pk})
CREATE TABLE E_TypeM(
	typeM AS ENUM ('Interception', 'Pillage', 'Combat', 'Transport')
);

-- -- -- -- -- -- -- -

-- Navire(codeNav {pk}, rayonNav, nbPilote, vitMax)
CREATE TABLE Navire(
	codeNav INT PRIMARY KEY AUTO_INCREMENT,
	rayonNav INT unsigned NOT NULL check (rayonNav > 0),
	nbPilote INT unsigned NOT NULL check (nbPilote > 0),
	vitMax INT unsigned NOT NULL check (vitMax > 0)
);

-- NavireTrsp(#codeNavTrsp {pk}, capacite)
CREATE TABLE NavireTrsp(
	codeNav INT PRIMARY KEY FOREIGN KEY REFERENCES Navire(codeNav),
	capacite INT unsigned NOT NULL check (capacite > 0)
);

-- NavireCbt(#codeNav {pk}, tailleMinEq, tailleMaxEq)
CREATE TABLE NavireCbt(
	codeNav INT PRIMARY KEY FOREIGN KEY REFERENCES Navire(codeNav),
	tailleMinEq INT unsigned NOT NULL check (tailleMinEq > 0),
	tailleMaxEq INT unsigned NOT NULL check (tailleMaxEq >= tailleMinEq)
);

-- NavireAtmo(#codeNav {pk}, #typeAtmo {pk})
CREATE TABLE NavireAtmo(
	codeNav INT FOREIGN KEY REFERENCES Navire(codeNav),
	typeAtmo Milieu,
	PRIMARY KEY (codeNav, typeAtmo)
);

-- -- -- -- -- -- -

-- Pilote(codePil {pk}, nomP, prenom, age, grade)
CREATE TABLE Pilote(
	codePil INT PRIMARY KEY AUTO_INCREMENT,
	nomP VARCHAR(100),
	prenom VARCHAR(100),
	age INT unsigned check (age > 0),
	grade VARCHAR(100) NOT NULL
);

-- PiloteForme(#codePil {pk}, #typeM {pk})
CREATE TABLE PiloteForme(
	codePil INT FOREIGN KEY REFERENCES Pilote(codePil),
	typeM E_TypeM,
	PRIMARY KEY (codePil, typeM)
);

-- PeutPiloteNav(#codePil {pk}, #codeNav {pk})
CREATE TABLE PeutPiloteNav(
	codePil INT FOREIGN KEY REFERENCES Pilote(codePil),
	codeNav INT FOREIGN KEY REFERENCES Navire(codeNav),
	PRIMARY KEY (codePil, codeNav)
);

-- -- -- -- -- -- -- -

-- Equipage(codeEquipage {pk}, nbEq)
CREATE TABLE Equipage(
	codeEquipage INT PRIMARY KEY AUTO_INCREMENT,
	nbEq INT unsigned NOT NULL check (nbEq > 0)
);

-- EquForme(#codeEqu {pk}, #typeM {pk})
CREATE TABLE EquForme(
	codeEquipage INT FOREIGN KEY REFERENCES Equipage(codeEquipage),
	typeM E_TypeM check (typeM != 'Transport'),
	PRIMARY KEY (codeEquipage, typeM)
);

-- -- -- -- -- -- -- -- -- -

-- Galaxie(codeGalaxie {pk}, nomG, distGR)
CREATE TABLE Galaxie(
	codeGalaxie INT PRIMARY KEY AUTO_INCREMENT,
	nomG VARCHAR(100) NOT NULL,
	distGR INT unsigned NOT NULL check (distGR > 0)
);

-- Planete(codePlanete {pk}, #codeGalaxie {pk}, nomP, vitLib, soumise, #typeAtmo)
CREATE TABLE Planete(
	codePlanete INT,
	codeGalaxie INT FOREIGN KEY REFERENCES Galaxie(codeGalaxie),
	nomP VARCHAR(100) NOT NULL,
	vitLib INT unsigned NOT NULL check (vitLib > 0)
	soumise BOOLEAN,
	typeAtmo Milieu,
	PRIMARY KEY(codeGalaxie, codePlanete)
);


-- -- -- -- -- -- -- -

-- Mission(codeMission {pk}, dateMi, nbNav, vitMinMi, rayonMinMi, #codeGalaxie)
CREATE TABLE Mission(
	codeMission INT PRIMARY KEY AUTO_INCREMENT,
	dateMi DATETIME,
	nbNav INT unsigned NOT NULL check (nbNav > 0),
	vitMinMi INT unsigned NOT NULL check (vitMinMi > 0),
	rayonMinMi INT unsigned NOT NULL check (rayonMinMi > 0),
	codeGalaxie INT NOT NULL FOREIGN KEY REFERENCES Galaxie(codeGalaxie)
);


-- MissionPlanete(#codeMission {pk}, #codePlanete)
	-- Rajouter #codeGalaxie ? (redondance)
CREATE TABLE MissionPlanete(
	codeMission INT PRIMARY KEY FOREIGN KEY REFERENCES Mission(codeMission),
	codePlanete INT NOT NULL FOREIGN KEY Planete(codePlanete)
);

-- AffectPilote(#codePil {pk}, #codeMission {pk}, #codeNav)
CREATE TABLE AffectPilote(
	codePil INT FOREIGN KEY REFERENCES Pilote(codePil),
	codeMission INT FOREIGN KEY REFERENCES Mission(codeMission),
	codeNav INT NOT NULL FOREIGN KEY REFERENCES Navire(codeNav),
	PRIMARY KEY (codePil, codeMission)
);

-- AffectEqu(#codeMission {pk}, #codeEquipe {pk} {sk}, #codeNavCbt {sk})
CREATE TABLE AffectEqu(
	codeMission INT FOREIGN KEY REFERENCES Mission(codeMission),
	codeEquipe INT FOREIGN KEY REFERENCES Equipage(codeEquipe),
	codeNavCbt INT NOT NULL FOREIGN KEY REFERENCES NavireCbt(codeNav),
	PRIMARY KEY (codeMission, codeEquipe),
	UNIQUE (codeEquipe, codeNavCbt)
);


rollback;