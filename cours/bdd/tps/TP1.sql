-- Question 1 :

select ns, noms, capch 
from resorts 
where typeS = 'montagne';

-- Question 2 :
select h.ns, nh, nomh, adrh, telh, cath 
from resorts r
	join hotels h on h.ns = r.ns
where types = 'mer';

-- Question 3 :
select distinct(r.ns), noms
from resorts r
	join hotels s on s.ns = r.ns
where cath = 4 and types = 'mer';

-- Question 4 :
select distinct(g.ncl), g.nomcl, g.adrcl
from guests g
	join bookings b on g.ncl = b.ncl 
	join resorts r on r.ns = b.ns
where typeS = 'montagne'; 

-- Question 5 :
select r.ns, r.nh, r.nch
from rooms r
	join hotels h on r.ns = h.ns and r.nh = h.nh 
	join resorts res on res.ns = r.ns
where types = 'montagne' and cath = 2 and prix < 50;

-- Question 6 :
select distinct(g.ncl), g.nomcl
from  guests g
	join bookings b on b.ncl = g.ncl
	join resorts res on b.ns = res.ns
	join rooms r on r.ns = b.ns and r.nh = b.nh and r.nch = b.nch
where types = 'mer' and (typCh = 'D' or typCh = 'DWC');  

-- Question 7 :
select distinct(g.ncl), g.nomcl
from guests g
	join hotels h on h.adrh = g.adrcl;

-- Question 8 :
select h.ns, h.nh
from hotels h
where cath = 4 and (h.ns, h.nh) not in (select r.ns, r.nh
										from rooms r
										where typCh != 'SDB');

-- Question 9 :
select distinct(h1.ns), h1.nh, h1.nomh, h1.adrh, h1.cath
from hotels h1
	join rooms r1 on h1.ns = r1.ns and h1.nh = r1.nh 
	join rooms r2 on r1.ns = r2.ns and r1.nh = r2.nh and r1.nch != r2.nch
where r1.prix = r2.prix;

-- Question 10 :
select h.ns, h.nh, h.nomh, adrh, cath, 0 as nbRes
from hotels h
where (h.ns,h.nh) not in (select b.ns, b.nh
							from bookings b)
union
select h.ns, h.nh, h.nomh, adrh, cath, count(*) as nbRes
from hotels h
	join bookings b on b.ns = h.ns and b.nh = h.nh
where b.ncl is not null
group by h.ns, h.nh, h.nomh, adrh, cath;


-- Question 11 :
select h.ns, h.nh, h.adrH
from hotels h
	join bookings b on b.ns = h.ns and b.nh = h.nh
where b.ncl is not null
group by h.ns, h.nh, h.nomh, adrh, cath
having count(*) = (select max(count(*))
					from hotels h
						join bookings b on b.ns = h.ns and b.nh = h.nh
						join resorts r on r.ns = h.ns
					where noms = 'Chamonix' and b.ncl is not null
					group by h.ns, h.nh, h.nomh, adrh, cath);

-- Question 12 :
select jour
from bookings b
	join rooms r on r.ns = b.ns and r.nh = b.nh and r.nch = b.nch
where (b.ns,b.nh) in (select h.ns, h.nh
					from hotels h
						join resorts res on res.ns = h.ns
					where res.noms = 'Chamonix' and h.nomh = 'Bon Séjour')
group by b.ns, b.nh, b.jour
having count(*) = (select max(count(*))
					from bookings b
						join rooms r on r.ns = b.ns and r.nh = b.nh and r.nch = b.nch
					where (b.ns,b.nh) in (select h.ns, h.nh
										from hotels h
											join resorts res on res.ns = h.ns
										where res.noms = 'Chamonix' and h.nomh = 'Bon Séjour')
					group by b.ns, b.nh, b.jour);


-- Question 13 :
select ns, nh, nomh, adrh, cath
from hotels
where (ns,nh) not in (select ns, nh
						from rooms
						where prix >= 40);

-- Question 14 :
select prix
from rooms r3
	join hotels h3 on h3.ns = r3.ns and h3.nh = r3.nh
	join resorts res3 on res3.ns = h3.ns
where cath = 3 and types = 'mer' and prix not in (select r1.prix
					from rooms r1
						join rooms r2 on r1.prix > r2.prix
					where (r1.ns, r1.nh, r1.nch) in 
							(select r.ns, r.nh, r.nch
							from rooms r
								join hotels h on h.ns = r.ns and h.nh = r.nh
								join resorts res on res.ns = h.ns
							where cath = 3 and types = 'mer')
						and (r2.ns, r2.nh, r2.nch) in (select r.ns, r.nh, r.nch
							from rooms r
								join hotels h on h.ns = r.ns and h.nh = r.nh
								join resorts res on res.ns = h.ns
							where cath = 3 and types = 'mer')
						);

-- Question 15 :
select g.ncl, g.nomcl
from guests g
	join bookings b on b.ncl = g.ncl
	join resorts res on res.ns = b.ns
	join hotels h on h.ns = b.ns and h.nh = b.nh
where res.noms = 'Chamonix' and cath = 2 
group by g.ncl, g.nomcl
having count(distinct b.nh) = (select count(*)
					from hotels h1
						join resorts res1 on res1.ns = h1.ns
					where res1.noms = 'Chamonix' and h1.cath = 2
					group by res1.ns);

-- Version alternative : WIP
select ns, nh, ncl
from bookings b 
where not exists 
	(select null from hotels where not exists 
		(select null from bookings where ))

-- Question 16 :
select g.ncl, g.nomcl
from guests g
where g.ncl in (select b.ncl
				from bookings b, bookings b1, bookings b2
				where b.ns = b1.ns and b1.ns = b2.ns and b.nh = b1.nh and b1.nh = b2.nh 
				and b.nch = b1.nch and b1.nch = b2.nch and b.ncl = b1.ncl and b1.ncl = b2.ncl
				and b.jour = ((b1.jour)-1) and b1.jour = ((b2.jour)-1));


select g.ncl, g.nomcl from guests g where g.ncl in (select b.ncl from bookings b, bookings b1, bookings b2 where b.ns = b1.ns and b1.ns = b2.ns and b.nh = b1.nh and b1.nh = b2.nh and b.nch = b1.nch and b1.nch = b2.nch and b.ncl = b1.ncl and b1.ncl = b2.ncl and b.jour = (b1.jour-1) and b1.jour = (b2.jour-1));
