
.. index::
   pair: Séminaires; Linux embarqué
   pair: Formation; Linux (embarqué)



.. _linux_embarque_9_mai_2012:

=================================================
Linux embarqué, Mercredi 9 mai 2012
=================================================



.. seealso::

   - http://www.phytec.fr


.. contents::
   :depth: 2

Confirmation
============

::

    Sujet:  Confirmation inscription Journée Technique Linux Embarqué
    Date :  Fri, 27 Apr 2012 10:01:00 +0200
    De :    Sylvie Berrard <s.berrard@phytec.fr>
    Pour :  patrick.vergain@id3.eu



Monsieur,


Nous avons le plaisir de confirmer votre participation à notre journée
technique Linux Embarqué du 09 mai à Lyon.

Nous serons heureux de vous accueillir à partir de 9h à l'hotel Kyriad.
Nous vous joignons le plan d'accès ainsi que les numéros de téléphone où
vous pourrez nous joindre le jour même:

Damien Barrier - 06 17 74 69 43
Hotel Kyriad - 04 78 37 16 64


En cas d'indisponibilité, merci de nous prévenir dans les meilleurs délais.

Nous restons bien sûr à votre entière disposition pour tout
renseignement complémentaire et dans l'attente de vous rencontrer,
veuillez agréer nos plus sincères salutations.

Plan d'accès
------------

:download:`Télécharger le plan d'accès pour Lyon <PLAN_ACCES_LYON.pdf>`


::

    PHYTEC France
    17, place St Etienne
    F-72140 Sillé-le-Guillaume
    TEL : + 33 (0)2 43 29 22 33
    FAX : + 33 (0)2 43 29 22 34
    e-mail : info@phytec.fr
    Internet : http://www.phytec.fr <http://www.phytec.fr/>


Invitation
===========


::


    Sujet:  Invitation journées techniques gratuites Linux
    Date :  Thu, 26 Apr 2012 16:28:53 +0200
    De :    PHYTEC France <info@phytec.fr>
    Pour :  Philippe Bourgault <philippe.bourgault@id3semiconductors.com>


Monsieur Philippe Bourgault,


La société PHYTEC, concepteur et fabricant de modules à microcontrôleur, propose
en partenariat avec CénoSYS, deux journées techniques gratuites sur le thème
Linux Embarqué.

Il reste quelques places disponibles sur la session du 9 mai à Lyon, n’hésitez
pas à vous inscrire, soit en nous appelant au 02.43.29.22.33, soit en
téléchargeant le bulletin d’inscription directement sur notre site internet.


Programme de la journée
-----------------------


- 9h00         Accueil des participants
- 9h30         Introduction
- 9h45         Présentation société Phytec et CénoSYS
- 10h00        Modules phyCARD, phyCORE et phyFLEX
- 10h45        Pause
- 11h15        Distribution Linux OSELAS.BSP() CORTEXA8
- 12h30        Déjeuner offert
- 13h45        Introduction aux drivers Linux
- 14h30        Démonstration sur module phyCARD-L
- 15h15        Pause
- 15h30        Application Qt4 Embedded
- 16h30        Démonstration sur cible OMAP
- 17h15        Debriefing / Conclusion


En espérant vous rencontrer bientôt à notre séminaire.
Sincères salutations




