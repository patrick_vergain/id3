
.. index::
   pair: Séminaires; Réussir son IHM



.. _reussir_son_ihm:

=================================================
Réussir son IHM, Mercredi 29 Février 2012
=================================================


.. seealso::

   - http://www.captronic.fr/Reussir-son-IHM.html
   - http://www.insavalor.fr/insavalor/Coordonnees.html
   - http://www.captronic.fr/spip.php?action=acceder_document&arg=1152&cle=cdc5ebfaeeb7d60d6ecb5b711d2d4f3e&file=pdf%2FProgramme-Seminaire-CAP-TRONIC-ReussirSonIHM.pdf



.. contents::
   :depth: 2

Annonce
========

.. seealso::

   - http://www.insavalor.fr/insavalor/Coordonnees.html

Dans le cadre du programme CAP’TRONIC dont l’objectif est d’aider les
PME à intégrer des solutions électroniques dans leurs produits, JESSICA
SUD-EST organise le 29 février 2012 un séminaire intitulé
Réussir son IHM

::

    De 13H30 à 17H30

    à INSVALOR, Campus LyonTech La Doua
    66, boulevard Niels Bohr - Bâtiment CEI
    69603 Villeurbanne




L’un des facteurs de différenciation et de succès des produits électroniques
passe par la qualité de l’Interface Homme Machine.

En effet, à l’ère des TIC, l’usage d’écrans est quasi permanent, que ce soit à
titre individuel : ordinateurs personnels, smartphones ou à titre public: bornes
de paiement, tickecting, distributeurs automatiques.

Cette omniprésence d’écrans oblige à concevoir des produits performants, faciles
à utiliser, plus intuitifs et plus adaptés aux usagers selon le contexte, grand
public ou professionnel par exemple.

Pourquoi l’iPhone est-il devenu une référence en la matière ? L’importance de la
conception centrée sur l’utilisateur doit être une préoccupation majeure dans le
processus de conception des interfaces.

Pour atteindre cet objectif, la coopération de trois disciplines s’impose : le
design pour l’attrait du produit, l’ergonomie pour la compatibilité du produit
avec une utilisation humaine et la technique pour la faisabilité.

Programme
=========

13h30
------

Accueil des participants

14h00 - 17h00 : Christophe BRUNSCHWEILER et Fabien VITIELLO (ESG)
------------------------------------------------------------------


.. seealso::

   - http://www.esg-group.fr/

- Introduction (20 min)
- Présentation d’ESG / de l’agenda, etc…
- Définition de ce qu’est une IHM (rendu + saisie)
- Nécessaire convergence entre métiers (15 min)
- Critères d’évaluation d’une IHM:

  * intuitivité,
  * facilité d’utilisation,
  * réactivité,
  * esthétisme

- Les différents métiers (Marketing, Designer, Ergonome, Technique pour l’implémentation)

- Problématique des IHMs (systèmes contraints) (25 min)
- Contraintes de sécurité
- Contraintes de sûreté de fonctionnement
- Contraintes d’environnement
- Contraintes d’utilisation
- Contraintes de marché

- Méthodes et solutions
- Méthodes pour répondre aux contraintes (30 min)

PAUSE
-----

3 exemples d’IHMs (= solutions techniques) (65 min)
----------------------------------------------------

- IHM optimisée en ressources
- IHM optimisée en rendu
- IHM du compromis


Conclusion et perspectives
--------------------------

17H30 : Cocktail
----------------

Cocktail


Liens
=====

- http://www-05.ibm.com/fr/events/developpement_si_2011/Partie1_Introduction_Problematique.pdf
-
