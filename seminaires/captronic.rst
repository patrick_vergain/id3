
.. index::
   ! Captronic


.. _captronic:

=================================================
Cap'tronic
=================================================


.. seealso::

   - http://www.captronic.fr
   - http://www.captronic.fr/spip.php?page=backend&id_mot=11


.. contents::
   :depth: 2


Tous les séminaires
===================

.. seealso:: http://www.captronic.fr/-Seminaires,22-.html


Coordonnées
===========

::

    Janique PERNOUD
    Assistante de la Direction Nationale

    Programme CAP'TRONIC
    JESSICA FRANCE
    CEA/G -Bât C1
    17 rue des Martyrs
    38054 GRENOBLE Cedex 9
    Tél +33 438789025 Fax +33 438785070
    pernoud@captronic.fr



Liste des séminaires
====================

- :ref:`reussir_son_ihm`
