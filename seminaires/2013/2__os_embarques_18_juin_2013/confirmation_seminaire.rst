
.. index::
   pair: Android; 2013
   pair: Entreprise; Adeneo



.. _os_embarques_18_juin_2013:

===================================================================================
Confirmation du séminaire le vendredi 31 mai 2013
===================================================================================


.. contents::
   :depth: 3

Courriel
=========

::

    PERNOUD Janique 052122 <janique.pernoud@cea.fr>
    à:	 "pvergain@gmail.com" <pvergain@gmail.com>
    date:	 31 mai 2013 14:47
    objet:	 RE: Inscription séminaire "Besoin d’un système d’exploitation ! Lequel choisir ?" 18 juin 2013
    envoyé par:	 cea.fr

     
Bonjour,

Votre inscription est confirmée
Cordialement,

Janique PERNOUD
================


Assistante de la Direction Nationale

::

    Programme CAP'TRONIC 
    JESSICA FRANCE
    CEA/G -Bât 51C
    17 rue des Martyrs
    38054 GRENOBLE Cedex 9
    Tél.  04.38.78.90.25  Fax. 04.38.78.50.70
    pernoud@captronic.fr
    http://www.captronic.fr


