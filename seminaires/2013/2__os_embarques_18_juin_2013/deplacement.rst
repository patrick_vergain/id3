

.. _transport_18_juin_2013:

===================================================================================
Transport
===================================================================================


.. contents::
   :depth: 3

Courriel
=========

::

    Date: Mon, 03 Jun 2013 10:48:12 +0200
    From: Patrick VERGAIN <patrick.vergain@id3.eu>
    Organization: id3 Technologies
    To: nathalie.delubac@id3.eu
    Subject: Billets de  train pour Saint-Etienne le 18 juin 2013

Bonjour Nathalie,

Je dois aller à Saint-Etienne le mardi 18 juin pour un séminaire qui
débute à 13h30 et termine à 18h30.

Comme le séminaire est à 20 minutes à pied (d'après google) de la gare
j'aimerai prendre une marge de sécurité (je n'ai jamais mis les pieds à
Saint-Etienne) et arriver au moins 1 heure avant c'est à dire avant
12h30 à Saint-Etienne.

Est-ce que tu peux me commander les billets de train ?


Liens
-----

- http://www.captronic.fr/Besoin-d-un-systeme-d-exploitation.html

- https://www.google.fr/maps?saddr=gare+%C3%A0+proximit%C3%A9+de+Saint-%C3%89tienne&daddr=25+Rue+Docteur+R%C3%A9my+Annino,+42000+Saint-%C3%89tienne&hl=fr&ie=UTF8&ll=45.44827,4.393115&spn=0.02514,0.066047&sll=45.449855,4.388794&sspn=0.006285,0.016512&geocode=FUFmtQIdDR9DACFa_wvJfqa1zim9PkNc_qv1RzFa_wvJfqa1zg%3BFVGCtQIdtfdCACnl6kKYBaz1RzHqfqh1jmbcaQ&oq=gare+saint+etienne&t=h&mra=atm&dirflg=w&z=15



::

    -- 
    Patrick

