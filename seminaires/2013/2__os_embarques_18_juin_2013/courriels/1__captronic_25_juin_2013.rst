

==========================================
Proposition de rendez vous avec Captronic 
==========================================

.. contents::
   :depth: 3



Courriel
========

::

    Date: Tue, 25 Jun 2013 10:50:04 +0200
    From: JLuc Baudouin <baudouin@captronic.fr>
    To: <patrick.vergain@id3.eu>
    Subject: Proposition de rendez vous avec Captronic

Bonjour Mr Vergain,

Suite à notre discussion à St Etienne, je vous propose de passer vous 
voir pour discutter de vos projets et d'éventuelles opportunités 
d'accompagnement.

Voici quelques dates en juillet :

- 8 juillet  14:00
- 17 juillet 14:00
- 18 juillet

merci de votre réponse,

cordialement,

::

    -- 
    Jean-Luc Baudouin- Captronic
    CAP'TRONIC - JESSICA France- Zone Minatec C51 p335
    38 054 Grenoble Cedex
    Tel : +33.4.38.78.53.05
    Mobile : +33.6.82.58.30.13
    www.captronic.fr
