

=========================
Programme 13h30 -> 18h30
=========================

.. contents::
   :depth: 3

13h30 Présentation des outils d’accompagnements de la CCI et de CAP’TRONIC
==========================================================================


14h00 Présentation des principaux systèmes d’exploitation de l’embarqué
==========================================================================

- Architecture de l’OS
- Les fonctions principales

14h30 Linux
===========

.. seealso::

   - http://fr.wikipedia.org/wiki/Linux-rt

15h10 Androïd
=============

15h50 WinCE
===========

Les sources sont disponibles.


16h30 Pause
===========

16h45 Quelques mots sur QNX et VxWorks
=======================================

17h15 Présentation d’une grille d’analyse comparative Androïd/Linux/Windows CE
==============================================================================

- Empreinte mémoire, 
- Temps de boot, 
- Libre de droits vs royalties, 
- Portage disponible, 
- Aspect temps réel, 
- Support HW, Technologies applicatives, 
- Pérennité
  
  
18h00 Retour d’expérience d’une PME : CIO Informatique
=======================================================

18h30 Conclusion 
================



