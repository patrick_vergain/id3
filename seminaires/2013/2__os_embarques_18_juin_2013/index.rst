
.. index::
   pair: Anthony ; Pellerin


.. _os_embarques_juin_2013:

================================================================================================
Cap’tronic 18 juin 2013 Besoin d’un système d’exploitation ! Lequel choisir ? CCI Saint-Etienne
================================================================================================


.. seealso::

   - http://www.captronic.fr/Besoin-d-un-systeme-d-exploitation.html

.. contents::
   :depth: 3

Introduction
=============

Cap’tronic, la CCI de St Etienne, Numélink et Telecom Saint-Etienne vous 
proposent un séminaire sur « comment choisir son OS »

Ce séminaire d’une demi-journée met le projecteur sur:

- Structure générale des OS
- Présentation de plusieurs types d’OS
- Grille d’analyse et de choix
- Retour d’expérience

Les applications embarquées sont de plus en plus construites sur base 
d’OS. 

Cette couche logicielle permet de bénéficier de services (ordonnancement, 
pile de communication, fichiers systèmes,…) qui permettent de développer 
de façon plus rapide, indépendamment de la cible, en associant plus de 
souplesse et une robustesse accrue.

Cependant, décider d’adopter un OS n’est pas un choix neutre et simple 
à faire. 

C’est un choix crucial, fortement dépendant de l’application, qui a un 
impact important sur l’organisation de l’équipe logicielle et les 
compétences à maitriser dans l’entreprise. 


Itinéraire
===========

.. seealso::

   - https://www.google.fr/maps?saddr=gare+%C3%A0+proximit%C3%A9+de+Saint-%C3%89tienne&daddr=25+Rue+Docteur+R%C3%A9my+Annino,+42000+Saint-%C3%89tienne&hl=fr&ie=UTF8&ll=45.44827,4.393115&spn=0.02514,0.066047&sll=45.449855,4.388794&sspn=0.006285,0.016512&geocode=FUFmtQIdDR9DACFa_wvJfqa1zim9PkNc_qv1RzFa_wvJfqa1zg%3BFVGCtQIdtfdCACnl6kKYBaz1RzHqfqh1jmbcaQ&oq=gare+saint+etienne&t=h&mra=atm&dirflg=w&z=15

Programme 13h30 -> 18h30
=========================

.. toctree::
   :maxdepth: 3
   
   programme/index

Les Intervenants
=================

Anthony Pellerin d'Adeneo Embedded
----------------------------------

.. seealso::

   - http://www.adeneo-embedded.com/
   - http://www.adeneo-embedded.com/Training/Android
   - https://twitter.com/AdeneoEmbedded

Christian Charreyre de CIO (Informatique Indutrielle)
-----------------------------------------------------   
   
- Christian Charreyre (CIO Informatique)
  A fait un exposé sur un de leurs projets
- Christian BERNARD (christian.bernard@CIOinfoindus.fr)
  Discussion 


Lieu 25, rue du Dr Remy Annino 42000 Saint-Etienne
===================================================

::

    TELECOM Saint Etienne
    Salle du Conseil
    25, rue du Dr Remy Annino
    42000 Saint-Etienne 
    
    

Confirmation du séminaire
==========================

.. toctree::
   :maxdepth: 3

   confirmation_seminaire


Courriels
============

.. toctree::
   :maxdepth: 3

   courriels/index
   

Déplacement
============

.. toctree::
   :maxdepth: 3

   deplacement
   
   







