

.. _android_16_avril_2013:

===================================================================================
Androïd (Jour 1) mardi 16 avril 2013
===================================================================================


.. contents::
   :depth: 3


Présentation d’Androïd
======================

Les versions
============

L’architecture d’Androïd
=========================

Les fonctionnalités de la plateforme Androïd
============================================

Les détails de la plateforme
============================

Aperçu du développement bas niveau sur Androïd
==============================================

- Compiler Androïd
- Porter Androïd sur une nouvelle carte
- Développement de drivers sous Androïd

Les outils
==========

- Présentation of the IDE Eclipse
- Installation of the IDE and the Androïd SDK
- Création d’un Device Emulator
- Profiling/Debug tools


Lab 1
======

Créer votre première application “Hello, World” en utilisant AVD.


Développement d’applications sous Androïd
==========================================

- Activités / Cycles de vie
- Gestion des menus
- Expérience utilisateur
- Listeners d’évènements
- Introduction à ADB (Androïd Debug Bridge)
- Gestion des ressources des applications
- BroadcastReceiver et notifications


Lab 2
======


Créer un réveil avec les notifications


