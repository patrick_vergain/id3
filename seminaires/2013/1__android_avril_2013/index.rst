
.. index::
   pair: Android; 2013
   pair: Entreprise; Adeneo



.. _android_avril_2013:

===================================================================================
ATELIER CAP’TRONIC : Androïd, Du 16 avril 09:00 au 18 avril 17:00 CCI Villefontaine
===================================================================================


.. seealso::

   - http://www.captronic.fr/ATELIER-CAP-TRONIC-Android,815.html
   - http://source.android.com/source/initializing.html

.. contents::
   :depth: 3

Introduction
=============

Poussé par les produits grand public (Smartphone, Tablette..), l’attente du marché
en matière d’interface graphique, de capacité multimédia est de plus en plus
exigeante et ce, quel que soit le marché, l’application ou le type de produit.

Androïd s’impose aujourd’hui comme une des références du domaine et tend à sortir
de la sphère de la téléphonie mobile pour s’étendre sur d’autres applications.

Cet atelier de trois jours s’adresse aux personnes qui souhaitent comprendre la
plateforme Androïd, apprendre à programmer et découvrir ce que cela implique de
porter Androïd dans des produits embarqués autre que des smartphones.

Intervenant : Adeneo Embedded Société d’ingénierie des systèmes embarqués
=========================================================================

.. seealso::

   - http://www.adeneo-embedded.com/
   - http://www.adeneo-embedded.com/Training/Android
   - https://twitter.com/AdeneoEmbedded


Confirmation du séminaire
==========================

.. toctree::
   :maxdepth: 3

   confirmation_seminaire


Déroulement ATELIER CAP’TRONIC
==============================

.. toctree::
   :maxdepth: 3

   1__mardi_16_avril/index
   2__mercredi_17_avril/index
   3__jeudi_18_avril/index







