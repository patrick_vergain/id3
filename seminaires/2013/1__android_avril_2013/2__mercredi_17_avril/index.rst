

.. _android_17_avril_2013:

===================================================================================
Androïd (Jour 2) mercredi 17 avril 2013
===================================================================================


.. contents::
   :depth: 3


Interface utilisateur Androïd
=============================

- Layout XML
- Hiérarchie des vues
- Styles / thèmes
- App Widgets
- Les contrôles
- Manifest
- Animations des contrôles
- Internationalisation


Lab 3
=====

Créer un convertisseur de monnaie supportant plusieurs résolutions


Les Services
=============

- Threads
- Services
- Asyns tasks
- IntentService
- Monitorer les applications avec LogCat
- Introduction à DDMS


Lab 4
======

Créer une application multithread et l’analyser avec DDMS.


Network
========

- Introduction aux sockets
- Requêtes HTTP
- REST and JSON WebServices
- Gestion des SMS et des emails
- Gestion de la stack Bluetooth
- Support géographique avec Google Map API


Lab 5
======

Créer un client Twitter


