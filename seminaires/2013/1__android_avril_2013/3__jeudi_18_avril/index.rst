

.. _android_18_avril_2013:

===================================================================================
Androïd (Jour 3) jeudi 18 avril 2013
===================================================================================


.. contents::
   :depth: 3


Data access
===========

- Options de stockage
- Les adapters
- Préférences utilisateur
- E/S sur les fichiers
- Base de données SQLite access
- Accès aux contacts


Lab 6
=====

Créer une application pour gérer les contacts du téléphone

Access to hardware
===================

- Gestion des évènements de tactile et de gesture
- Caméra
- Gestion des capteurs
- Enregistrement de l’audio & playback
- Gestion du WIFI


Lab 7
======

Créer une application pour prendre des photos, détecter des visages, enregistrer
un commentaire audio et géolocaliser le Smartphone

Monétariser votre App
=====================

- Présentation du Google Play
- Publication et mise à jour des applications
- Achat d’applications In-App


Best practices
===============

- Support de multiples résolutions
- Java Debugging
- Optimisation pour Androïd 4.0
- UI Guidelines
- Astuces pour l’émulateur


Conclusion
===========


