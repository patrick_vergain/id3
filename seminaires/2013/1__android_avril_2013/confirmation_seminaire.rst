

=======================================================================================
Confirmation inscription : Atelier ANDROID des 16/17 et 18 avril 2013 à Villefontaine
=======================================================================================


.. contents::
   :depth: 3

Introduction
=============


::

    PERNOUD Janique 052122 <janique.pernoud@cea.fr>
    cc:  "baudouin@captronic.fr" <baudouin@captronic.fr>
    date:    12 avril 2013 10:31
    objet:   Confirmation inscription : Atelier ANDROID des 16/17 et 18 avril 2013 à Villefontaine
    envoyé par:  cea.fr


Bonjour,

Nous vous confirmons que vous êtes attendu à partir de 8h45 pour participer à
l'atelier « ANDROID » qui aura lieu les 16-17 et 18 avril 2013 à Villefontaine.

Chaque participant viendra avec un PC portable, disposant de 10 Go de libre sur
le disque dur et disposant des droits administrateurs sur son PC.

Les déjeuners seront pris en commun et sont offerts par le programme CAP’TRONIC.


Intervenant
============

ADENEO EMBEDDED


Date : 16-17 et 18 avril 2013 de 9h00 à 17h00
=============================================


Lieu : Chambre de Commerce et d'Industrie de Villefontaine
===========================================================


::


    Chambre de Commerce et d'Industrie de Villefontaine
    Parc Technologique
    5 Rue condorcet - BP 108
    38093 Villefontaine Cedex
    Tél. 04 74 95 24 00
    Fax : 04 74 95 24 01


Institut Supérieur de formation ``ISFO Nord Isère``
---------------------------------------------------

::

    ISFO Nord Isère
    Institut Supérieur de Formation
    Parc Technologique
    5 Rue condorcet - BP 108
    38093 Villefontaine Cedex
    Tél. 04 74 95 24 20
    Fax : 04 74 95 54 42

Plan d'accès
------------

Plan d'accès à l'Institut Supérieur de formation de Villefontaine.


.. figure:: CCI_Nord_Isere_plan_d_acces.jpg
   :align: center

(En cas d'empêchement merci de nous prévenir au plus vite svp)
A votre disposition pour tout complément d'information.
Cordialement,

::

    Janique PERNOUD
    Assistante de la Direction Nationale
    Programme CAP'TRONIC

    JESSICA FRANCE
    CEA/G -Bât C1
    17 rue des Martyrs
    38054 GRENOBLE Cedex 9

    Tél.  04.38.78.90.25  Fax. 04.38.78.50.70

    pernoud@captronic.fr
    http://www.captronic.fr





