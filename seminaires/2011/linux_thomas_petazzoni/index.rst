
.. index::
   pair: Formation; Linux (2011)
   pair: Thomas ; Petazzoni
   pair: linux people; Thomas Petazzoni
   pair: Système embarqué; Thomas Petazzoni
   pair: Système embarqué; buildroot
   ! Thomas Petazzoni




.. _seminaire_linux_petazzoni_2011:

=================================================
Séminaire Linux Thomas Petazzoni (2011)
=================================================


.. _thomas_petazzoni:

Thomas Petazzoni
=======================

.. seealso::

   - http://www.guilde.asso.fr/rencontres/20091013/
   - https://plus.google.com/u/0/101327154101389327284
   - http://buildroot.uclibc.org/git.html
   - https://twitter.com/free_electrons


Présentation
============

Thomas Pettazzoni est un des principaux développeurs de Buildroot ,
un outil de construction de systèmes Linux embarqué, et utilisateur/développeur
sous système Linux depuis plus de dix ans.


Chez Free electrons
===================

.. seealso::

   - http://free-electrons.com/company/staff/thomas-petazzoni/


Presentation CapTronic
=============================

:download:`Présentation CapTronic <CapTronic-INPI_2011.pdf>`.



Presentation Thomas Petazzoni
=============================

:download:`Présentation de Thomas Petazzoni <presentation_thomas_petazzoni_free_electrons.pdf>`.



