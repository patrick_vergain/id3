

.. index::
   pair: Outils; Sphinx
   pair: Doc; Sphinx


.. _doc_sphinx:

===========================================
Production de la documentation avec sphinx
===========================================

.. contents::
   :depth: 3


Introduction
============

La documentation a été produite au moyen du logiciel sphinx_.

.. _sphinx:  http://sphinx.pocoo.org/latest/index.html


Commande de production de la documentation: ``make html``
=========================================================

::

    make html


``make html``
-------------

.. figure:: make_html.png
   :align: center

   Commande pour produire la documentation







