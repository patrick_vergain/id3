
.. index::
   ! Read the docs

.. _id3_on_read_the_docs:

=================================================
id3technologies on read the docs
=================================================


.. seealso::

   - https://readthedocs.org/dashboard/

.. mdp Thw4Dr!879



Introduction
============

.. seealso::

   - http://www.marinamele.com/taskbuster-django-tutorial/documenting-project-github-readthedocs
   
   
Upload your Docs on ReadTheDocs

First, create an account in ReadTheDocs if you don’t have one yet.

Next, you can connect your GitHub account and import your project’s docs from 
there.

And you just need to import your desired project from GitHub, and create your 
docs in ReadTheDocs!

You can see the results here!



Github account
===============

.. seealso:: 

   - :ref:`id3_on_github`
   - https://readthedocs.org/dashboard/import/github/sync/
   - http://www.soccermetrics.net/software-development/how-we-automate-our-software-documentation-using-sphinx-bootstrap-and-github-pages
   
   


