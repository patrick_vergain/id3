


.. _windows_8.1:

=================================================
Windows 8.1
=================================================



::

    Return-Path: 	<pierre.marc@id3.eu>
    Delivered-To: 	tout_id3@id3.eu
    Received: 	from lamier.gen.id3.lan (lamier.gen.id3.lan [192.168.202.24]) by mail-srv.gen.id3.lan (Postfix) with ESMTP id AB0D1807A8 for <tout_id3@id3.eu>; Fri, 8 Nov 2013 11:52:38 +0100 (CET)
    Date: 	Fri, 08 Nov 2013 11:52:38 +0100
    From: 	Pierre MARC <pierre.marc@id3.eu>
    Organization: 	id3 Semiconductors
    To: 	tout_id3 <tout_id3@id3.eu>
    Subject: 	MAJ Windows 8.1


Le tutoriel en français et très détaillé :
http://www.cnetfrance.fr/news/installer-windows-81-sans-passer-par-le-windows-store-39795226.htm

Clés génériques :
Core: 334NH-RXG76-64THK-C7CKG-D3VPT
Professional: XHQ8N-C3MCJ-RQXB6-WCHYG-C9WKB
Enterprise: MNDGV-M6PKV-DV4DR-CYY8X-2YRXH
FHQNR-XYXYC-8PMHT-TV4PH-DRQ3H

gvlkCore = M9Q9P-WNJJT-6PXPY-DWX8H-6XWKK
gvlkProfessional = GCRJD-8NW9H-F2CDX-CCM8D-9D6T9

-- 
Pierre MARC
id3 Technologies
+33.476.75.75.85
