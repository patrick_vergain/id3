
.. index::
   pair: Outil; Subversion
   pair: Outil; svn
   pair: Subversion ; Biométrie
   ! Subversion


.. _subversion:

=================================================
Apache Subversion
=================================================

.. seealso::

   - http://subversion.apache.org/
   - http://svnbook.red-bean.com
   - http://fr.wikipedia.org/wiki/Apache_Subversion
   - http://code.google.com/p/svnbook/
   - http://twistedmatrix.com/trac/wiki/UltimateQualityDevelopmentSystem
   - http://pvbookmarks.readthedocs.org/en/latest/devel/vcs/subversion/index.html


.. figure:: subversion_logo.png
   :align: center

   *Subversion logo*

.. contents::
   :depth: 3


Introduction
============

Apache Subversion (often abbreviated SVN, after the command name svn) is a
software versioning and a revision control system distributed under a free
license.

Developers use Subversion to maintain current and historical versions of files
such as source code, web pages, and documentation.

Its goal is to be a mostly-compatible successor to the widely used
Concurrent Versions System (CVS).



svn externals
===================

.. toctree::
   :maxdepth: 3
   
   externals/externals
   
   
   
   
