
.. index::
   pair: Externals; Subversion


.. _subversion_externals:

=================================================
Subversion externals
=================================================

.. contents::
   :depth: 3


.. contents::
   :depth: 3


Description
===========

::

    svn:externals      - A list of module specifiers, one per line, in the
      following format similar to the syntax of 'svn checkout':
        [-r REV] URL[@PEG] LOCALPATH
      Example:
        http://example.com/repos/zig foo/bar
      The LOCALPATH is relative to the directory having this property.
      To pin the external to a known revision, specify the optional REV:
        -r25 http://example.com/repos/zig foo/bar
      To unambiguously identify an element at a path which may have been
      subsequently deleted or renamed, specify the optional PEG revision:
        -r25 http://example.com/repos/zig@42 foo/bar
      The URL may be a full URL or a relative URL starting with one of:
        ../  to the parent directory of the extracted external
        ^/   to the repository root
        /    to the server root
        //   to the URL scheme
      Use of the following format is discouraged but is supported for
      interoperability with Subversion 1.4 and earlier clients:
        LOCALPATH [-r PEG] URL
      The ambiguous format 'relative_path relative_path' is taken as
      'relative_url relative_path' with peg revision support.
      Lines starting with a '#' character are ignored.


::


    svn propget svn:externals .

::

    $ svn propget svn:externals .
    
    ^/../P3N161/Concept/Common common
    ^/../P3N161/Concept/LIB_2P147_id3Image/2.0.0/Livraison native/id3Image
    ^/../P3N161/Concept/LIB_9X164_FFT/Livraison native/fftw
    ^/../P3N161/Concept/LIB_3U142_id3.Controls/1.0/Livraison dotNET/id3.Controls
    ../../LIB_9Z119_id3FingerBase/2.4     native/id3FingerBase
    ../../LIB_9Y440_id3FingerEnroll/2.2   native/id3FingerEnroll
    ../../LIB_9X168_id3FingerFIR/1.4      native/id3FingerImageRecord
    ../../LIB_9X167_id3FingerMatch/2.4    native/id3FingerMatch
    ../../LIB_9X171_id3FingerImage/1.3    native/id3FingerImage
    ../../LIB_9X170_id3FingerTemplate/1.5 native/id3FingerTemplate
    ../../LIB_9Z231_id3FingerScan/1.4     native/id3FingerCapture
    ../../LIB_9X165_id3FingerExtract/3.0  native/id3FingerExtract
    ../../LIB_9X166_id3FingerFMR/1.4      native/id3FingerTemplateRecord
    ../../LIB_1N106_id3FingerNFIQ/1.2     native/id3FingerNFIQ
    ../../LIB_1S056_id3FingerLicense/2.6  native/id3FingerLicense
    ../../LIB_1W036_id3FingerSlapSeg/2.1  native/id3FingerSlapSeg
    ../../LIB_2W008_id3FingerWSQ/1.2      native/id3FingerWSQ
    ../../LIB_3R110_id3FingerBSP/1.0      native/id3FingerBSP


See the :ref:`externals files examples <svn_externals_examples>`:


::

    svn propset svn:externals  . -F externals.txt
    svn up



Usage de SVN en biométrie
==========================

::

    From:   - Mon, 20 Oct 2014 08:18:21 GMT
    Date:   Mon, 20 Oct 2014 08:18:21 GMT
    Message-Id:     <http://redmine/boards/25/topics/323@localhost.localdomain>
    From:   Christophe CANDELA <christophe.candela@id3.eu>
    MIME-Version:   1.0
    Subject:    Développement: [SVN] Méthodologie


Pour rappel, tout projet dans SVN (logiciel, documentation, etc) doit avoir :

- un code chrono
- un numéro de version
- un sous-répertoire 'livraison' pointant sur ^/_livraisons/<nom_projet>/<version> 
  (en utilisant svn:external)

Lors de la création d'un nouvelle version (branche) pour un projet, bien penser à :

- créer la branche aussi dans _livraisons
- mettre à jour l'external pour pointer sur la bonne version



Exemples
=========

.. toctree::
   :maxdepth: 3


   exemples/exemples
