
.. index::
   pair: svn externals  ; Examples


.. _svn_externals_examples:

=================================================
svn externals file
=================================================

.. contents::
   :depth: 3


Pour les utiliser
=================

::

    svn propset svn:externals  . -F externals.txt
    svn up


Exemple 1
=========

.. literalinclude:: externals_1.txt
   :linenos:

Exemple 2
=========

.. literalinclude:: externals_2.txt
   :linenos:


Exemple 3
=========

.. literalinclude:: externals_3.txt
   :linenos:


Exemple 4
=========

.. literalinclude:: externals_4.txt
   :linenos:



Pour les utiliser
=================

::

    svn propset svn:externals  . -F externals.txt
    svn up

