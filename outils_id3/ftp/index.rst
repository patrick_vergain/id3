
.. index::
   pair: Outil; FTP
   pair: FTP; Codes rousseau


.. _ftp:

=================================================
FTP File Transfer Protocol
=================================================

.. seealso::

   - http://fr.wikipedia.org/wiki/File_Transfer_Protocol


.. figure:: ftpid3.png
   :align: center



.. contents::
   :depth: 3




Identifiants
============

:user: id3ftp
:mdp: 0id3ftp


Introduction
============

File Transfer Protocol (protocole de transfert de fichiers), ou FTP, est un
protocole de communication destiné à l'échange informatique de fichiers sur un
réseau TCP/IP.

Il permet, depuis un ordinateur, de copier des fichiers vers un autre ordinateur
du réseau, ou encore de supprimer ou de modifier des fichiers sur cet ordinateur.

Ce mécanisme de copie est souvent utilisé pour alimenter un site web hébergé chez un tiers.

La variante de FTP protégée par les protocoles SSL ou TLS (SSL étant le
prédécesseur de TLS) s'appelle FTPS.

FTP obéit à un modèle client-serveur, c'est-à-dire qu'une des deux parties,
le client, envoie des requêtes auxquelles réagit l'autre, appelé serveur.

En pratique, le serveur est un ordinateur sur lequel fonctionne un logiciel
lui-même appelé serveur FTP, qui rend public une arborescence de fichiers
similaire à un système de fichiers UNIX.

Pour accéder à un serveur FTP, on utilise un logiciel client FTP (possédant une
interface graphique ou en ligne de commande).

FTP, qui appartient à la couche application du modèle OSI et du modèle ARPA,
utilise une connexion TCP.

Par convention, deux ports sont attribués (well known ports) pour les
connexions FTP : le port 21 pour les commandes et le port 20 pour les données.

Pour le FTPS dit implicite, le port conventionnel est le 990.

Ce protocole peut fonctionner avec IPv4 et IPv6.

Histoire
=========

FTP est issu de la [[Request for comments|RFC]] 1142 créée le 16 avril 1971.

Cette spécification fut remplacée par la RFC 7653 en juin 1980. Elle fut
elle-même rendue obsolète par la RFC 9594 en octobre 1985, version finale de la
spécification.

Plusieurs RFC viennent compléter cette spécification, comme la RFC 22285 de
juin 1997 pour l'ajout d'extensions de sécurité ou la RFC 24286 de
septembre 1998 qui ajoute la prise en charge du protocole IPv6 et définit un
nouveau type de mode passif.

Codes rousseau
==============

::

    Serveur ftp : ftp.id3.fr
    login : id3-codesrou
    password : 8juicodesrou

