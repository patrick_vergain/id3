
.. index::
   pair: Outil; Visual Studio
   pair: 2010; Visual Studio

.. _visual_studio:

=================================================
Visual Studio
=================================================


.. toctree::
   :maxdepth: 3


   addins/index
   versions/index

