
.. index::
   pair: DaemonTools; Visual Studio
   pair: 2012; Visual Studio

.. _visual_studio_2012:

=================================================
Visual Studio 2012
=================================================


.. seealso::

   - :ref:`daemon_tool`


Introduction
============


Visual studio 2012 se trouve sous:

- :file:`H:/outils_developpement/VisualStudio2012/fr_visual_studio_2012_professional_x86_dvd_519327.iso`


Daemon tools se trouve sous:

- :file:`H:/outils_developpement/Windows_utils/lecture_images_iso/daemon-tools-lite_daemon_tools_lite_4.46.1.0328_francais_10729.exe`



.. _install_visual_studio_2012:

Installation de visual studio 2012
==================================


DaemonTools étant au préalable installé, cliquer sur
fr_visual_studio_2012_professional_x86_dvd_519327.iso

