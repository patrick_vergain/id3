
.. index::
   pair: Versions; Visual Studio

.. _visual_studio_versions:

=================================================
Visual Studio versions
=================================================


.. toctree::
   :maxdepth: 3


   2012/index
   2010/index

