
.. index::
   pair: DaemonTools; Visual Studio
   pair: 2010; Visual Studio

.. _visual_studio_2010:

=================================================
Visual Studio 2010
=================================================


.. seealso::

   - :ref:`daemon_tool`


Introduction
============


Visual studio 2010 se trouve sous:

- :file:`H:/outils_developpement/VisualStudio2010/fr_visual_studio_2010_professional_x86_dvd_519327.iso`


Daemon tools se trouve sous:

- :file:`H:/outils_developpement/Windows_utils/lecture_images_iso/daemon-tools-lite_daemon_tools_lite_4.46.1.0328_francais_10729.exe`



.. _install_visual_studio_2010:

Installation de visual studio 2010
==================================


DaemonTools étant au préalable installé, cliquer sur
fr_visual_studio_2010_professional_x86_dvd_519327.iso

