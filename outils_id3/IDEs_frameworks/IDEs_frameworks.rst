
.. index::
   pair: Id3; IDEs
   pair: Id3; Frameworks


.. _ide_utilises:
.. _frameworks_utilises:

=================================================
IDEs/frameworks utilisés
=================================================


.. toctree::
   :maxdepth: 3


   eclipse/index
   netbeans/index
   qt/qt
   visual_studio/index
