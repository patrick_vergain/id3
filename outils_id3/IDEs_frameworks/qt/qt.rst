
.. index::
   pair: IDE; Qt
   ! Qt

.. _qt:

=================================================
Qt framework
=================================================


.. seealso::

   - http://fr.wikipedia.org/wiki/Qt
   - http://wiki.qt-project.org/Main_Page
   - http://qt-project.org/
   - http://qt-apps.org/
   - http://qt.developpez.com/faq/


.. contents::
   :depth: 3

Introduction
============

Qt (/ˈkjuːt/ "cute", or unofficially as Q-T cue-tee) is a cross-platform application
framework that is widely used for developing application software with a graphical
user interface (GUI) (in which cases Qt is classified as a widget toolkit), and
also used for developing non-GUI programs such as command-line tools and consoles
for servers.

Qt uses standard C++ but makes extensive use of a special code generator
(called the Meta Object Compiler, or moc) together with several macros to enrich
the language.

Qt can also be used in several other programming languages via language bindings.

It runs on the major desktop platforms and some of the mobile platforms.

It has extensive internationalization support.

Non-GUI features include SQL database access, XML parsing, thread management,
network support, and a unified cross-platform application programming interface
(API) for file handling.



Qt creator
==========

.. toctree::
   :maxdepth: 3

   qt_creator/qt_creator


Versions
========

.. toctree::
   :maxdepth: 3

   versions/versions
