
.. index::
   pair: DaemonTools; Lecteurs CD
   pair: Outil; DaemonTools


.. _daemon_tool:

=================================================
DAEMON Tools
=================================================


.. seealso::

   - http://fr.wikipedia.org/wiki/DAEMON_Tools
   - http://www.daemon-tools.cc/fra/home
   - http://www.clubic.com/telecharger-fiche10729-daemon-tools.html


.. figure:: Daemon-tools.png
   :align: center

.. contents::
   :depth: 3

Introduction
============

DAEMON Tools est un émulateur de lecteur CD / DVD, c'est-à-dire un programme
informatique capable de créer des lecteurs virtuels afin de permettre la lecture
d'images de disques comme s'il s'agissait de supports physiques.

Il est disponible pour le système d'exploitation Microsoft Windows, de la
version 2000 à Windows 7.


DaemonTools est LE logiciel de l'émulation CD-ROM ! Il permet, comme
beaucoup d'autres, d'émuler un ou plusieurs lecteurs sur votre PC mais
se démarque ensuite par le nombre d'options qu'il propose. Il est ainsi
possible de monter la plupart des types d'images disques existants (CUE,
ISO, CCD...)


Utilisation
===========

- :ref:`install_visual_studio_2010`
