
.. index::
   pair: Installateur; Wise


.. _installateur_wise:

==================
Installateur Wise
==================


.. contents::
   :depth: 3


Passage à une version supérieure
================================

Pour passer à une version suivante il faut:

Dans le menu ``Project Definition/Products Details``
-----------------------------------------------------

- sélectionner ``Product code``
- cliquer sur Change
- cliquer sur 'No'

.. figure:: upgrade_existing_product.png
   :align: center


Dans le menu ``Distribution/Upgrades``
---------------------------------------

- sélectionner la ligne qui contient l'upgrade code
- indiquer quelles versions sont concernées


.. figure:: upgrade_details.png
   :align: center
