
.. index::
   pair: Outil; Orcad
   pair: Outil; Cadence

.. _orcad_tool:

=================================================
Orcad (Oregon + CAD)
=================================================

.. seealso::

   - http://en.wikipedia.org/wiki/OrCAD
   - http://en.wikipedia.org/wiki/Cadence_Design_Systems

.. contents::
   :depth: 3


Introduction
============

OrCAD is a proprietary software tool suite used primarily for electronic design
automation.

The software is used mainly by electronic design engineers and electronic
technicians to create electronic schematics and electronic prints for manufacturing
printed circuit boards.

The name OrCAD is a portmanteau, reflecting the company and its software's
origins: Oregon + CAD.


History
=======

Founded in 1985 by John Durbetaki, Ken and Keith Seymour as “OrCAD Systems Corporation”
in Hillsboro, Oregon, the company became a leading worldwide supplier of desktop
electronic design automation (EDA) software.

"Wouldn't it be nice if there were a CAD program that was both inexpensive and
would run in the IBM Personal Computer (PC)?

John Durbetaki thought so back in 1984, when he began designing an expansion
chassis for the IBM PC. Durbetaki, who had left Intel Corp. after five years as
an engineer and project manager, decided, along with brothers Keith and Ken Seymour,
to start his own company to develop add-on instrumentation for the PC.

"[1] Durbetaki began creating his own schematic capture tool for his use in the
PC expansion chassis project; eventually, the hardware project was shelved
entirely in favor of developing low-cost, PC-based CAD software.

The company's first product was SDT (Schematic Design Tools), which shipped for
the first time late in 1985.

In 1986, OrCAD hired Peter LoCascio to develop sales and co-founder Ken Seymour
left the company. The flagship SDT product was soon followed with a digital
simulator, VST (Verification and Simulation Tools) and printed circuit board
(PCB) layout tools.

Over time, OrCAD's product line broadened to include Windows-based software
products to assist electronics designers in developing field-programmable gate
arrays (FPGAs), including complex programmable logic devices (CPLDs).

Durbetaki, then CEO and head of R&D, left the company in the 1990s.
He was succeeded as CEO by Michael Bosworth.

In June 1995, OrCAD acquired Massteck Ltd., a small company that offered
a printed circuit board layout tool and a sophisticated autorouter,[5] and
Intelligent Systems Japan, KK, OrCAD's distributor in Japan.

In 1996, OrCAD made a public offering. In late 1997 and early 1998, OrCAD and
Irvine, CA-based MicroSim Corp. (a supplier of PC-based analog and mixed-signal
simulation software for designing printed circuit board systems) merged, a
business combination that ultimately proved to be disappointing.[8][9]

In 1999, the company and its products were acquired by one of its former competitors,
Cadence Design Systems.

Since 16 July 1999, OrCAD's product line has been fully owned by Cadence Design Systems.

OrCAD Layout has been discontinued. The latest iteration of OrCAD CIS schematic
capture software has the ability to maintain a database of available integrated
circuits.

This database may be updated by the user by downloading packages from component
manufacturers, such as Analog Devices or Texas Instruments.

Another announcement was that ST Microelectronics will offer OrCAD PSpice models
for all the power and logic semiconductors, since PSpice is the most used
circuit simulator. Intel offers reference PCBs designed with Cadence PCB Tools
in the OrCAD Capture format for embedded and personal computers.




