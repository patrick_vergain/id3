

.. index::
   pair: Outils; Skype
   ! Skype


.. _skype:

===========================================
Skype
===========================================


.. seealso::

   - https://fr.wikipedia.org/wiki/Skype

.. contents::
   :depth: 3


.. figure:: Skype_logo.svg.png
   :align: center
   
  
Description
============
   
Skype est un logiciel gratuit qui permet aux utilisateurs de passer des appels 
téléphoniques via Internet. 

Les appels d’utilisateur à utilisateur sont gratuits, tandis que ceux vers les 
lignes téléphoniques fixes et les téléphones mobiles sont payants. 

Il existe des fonctionnalités additionnelles comme la messagerie instantanée, 
le transfert de fichiers et la visioconférence.

   
Sécurité et polémiques
======================

Skype est à l’origine d’un débat sur la sécurité des communications par la 
technique de la voix sur IP et de la sécurité liée au logiciel Skype lui-même. 

Les questions majeures proviennent de ce que Skype utilise certains utilisateurs 
pour relayer les communications d’autres utilisateurs et que Skype est une 
application totalement fermée utilisant des protocoles fermés, et donc non 
vérifiables, pour sécuriser son trafic.

En janvier 2013, ce manque de transparence sur le logiciel en lui-même et sur 
les liens commerciaux de la société a été critiqué dans une lettre ouverte 
rédigée par plusieurs associations.

Skype est proscrit par le centre national de la recherche scientifique dans les 
laboratoires sensibles mais « il peut être toléré une utilisation de Skype sur 
une machine dédiée et déconnectée du réseau du laboratoire dans la mesure où 
les informations échangées ne sont pas sensibles ».

Le compte utilisateur permettant de se connecter à Skype ne peut pas être 
supprimé, l'utilisateur a seulement la possibilité d'effacer ses informations 
personnelles de son profil afin que l'on ne le trouve plus dans l'annuaire.

Skype est cité le 12 juillet 2013 dans un article de The Guardian23 concernant 
les révélations sur le programme de surveillance PRISM_.

.. _PRISM:  https://fr.wikipedia.org/wiki/PRISM_%28programme_de_surveillance%29   
