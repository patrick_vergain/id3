@echo Let's go !
@echo ... Deliver the id3ClPCSCReader_CL1356A pack

@echo copy the include files
copy  XLOG8X195_id3ContactlessPCSCReader\id3ContactlessPCSCReader\Include\*.h  Livraison\SDK\Include

@echo copy the def files
copy XLOG8X195_id3ContactlessPCSCReader\id3ContactlessPCSCReader\export.def  Livraison\SDK\export.def

@echo copy the manual DOC files
copy  docs\XLOG9S039_Doc_manual\FR\PDF\*.*  Livraison\Doc\FR
copy  docs\XLOG9S039_Doc_manual\EN\PDF\*.*  Livraison\Doc\EN

@echo smartcard storage
copy external_resources\smartcard_storage\EXT_NullStorageCard\*.*  Livraison\null_storage_card

@echo setuplauncher
copy external_resources\setuplauncher\EXT_SetupLauncher\SetupLauncher.exe  Livraison

@echo copy the installer
copy XLOG8X220_Wise1356A\OrcanthusContactlessCL1356AReader.exe  Livraison\OrcanthusContactLessReader\OrcanthusContactlessCL1356AReader.exe

@echo ...
@echo That's all folks !

pause





