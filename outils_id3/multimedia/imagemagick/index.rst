

.. index::
   pair: Outil; ImageMagick
   pair: Convert; Image


.. _imagemagick:

==================================
ImageMagick
==================================

.. seealso::

   - http://www.imagemagick.org/www/binary-releases.html#windows 


.. figure:: logo_image_magick.jpg
   :align: center
   
   
Introduction
============


``ImageMagick`` est un logiciel libre, comprenant une bibliothèque, ainsi qu'un 
ensemble d'utilitaires en ligne de commande, permettant de créer, de convertir, 
de modifier et d'afficher des images dans un très grand nombre de formats. 

Les images peuvent être découpées, les couleurs peuvent être modifiées, 
différents effets peuvent être appliqués aux images, les images peuvent 
subir des rotations, il est possible d'y inclure du texte, des segments, 
des polygones, des ellipses et des courbes de Bézier, etc.
   


Exemple d'utilisation
=====================

Suppression d'une bande de 12 pixels sur 7 images.

::

    convert image_1.png -crop 174x131+0+12 crop\image_1.png
    convert image_2.png -crop 174x131+0+12 crop\image_2.png
    convert image_3.png -crop 174x131+0+12 crop\image_3.png
    convert image_4.png -crop 174x131+0+12 crop\image_4.png
    convert image_5.png -crop 174x131+0+12 crop\image_5.png
    convert image_6.png -crop 174x131+0+12 crop\image_6.png
    convert image_7.png -crop 174x131+0+12 crop\image_7.png

       
   
    
