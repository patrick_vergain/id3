
.. index::
   pair: Multimedia; Outils


.. _outils_multimedia_utilises:

=================================================
Outils multimedia utilisés
=================================================


.. toctree::
   :maxdepth: 3


   gimp/index
   imagemagick/index
   inkscape/index

