

.. index::
   pair: Outil; Gimp


.. _gimp:

=====================================
GIMP (GNU Image Manipulation Program)
=====================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Gimp
   - http://www.gimp.org/



.. figure:: 80px-GIMP_Icon.svg.png
   :align: center


.. contents::
   :depth: 3

Introduction
==============

GIMP (GNU Image Manipulation Program) est un outil d'édition et de retouche 
d'image et est diffusé sous la licence GPLv3 comme un logiciel gratuit et libre. 

Il en existe des versions pour la plupart des systèmes d'exploitation dont 
GNU/Linux, OS X, et Microsoft Windows.

GIMP a des outils utilisés pour la retouche et l'édition d'image, le dessin à 
main levée, réajuster, rogner, photomontages, convertir entre différents formats 
d'image, et plus de tâches spécialisées. 

Les images animées comme les fichiers GIF et MPEG peuvent être créées en 
utilisant un plugin d'animation.

Les développeurs et mainteneurs de GIMP souhaitent créer un logiciel d'infographie 
gratuit haut de gamme pour l'édition et la création d'images originales, 
de photos, d'icônes, d'éléments graphiques de pages web, et d'art pour les 
éléments de l'interface de l'utilisateur.

     
Utilisation
===========


``GIMP`` a été utilisé pour:

- faire des copies d'écran des fichiers PowerPoint fournis par Wavelens pour prototyper
  l'interface graphique
- redimensionner ces copies d'écran 


.. figure:: utilisation.png
   :align: center
   
   

  
     
       
   
    
