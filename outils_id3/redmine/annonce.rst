

=====================
Annonce outil Redmine
=====================

.. contents::
   :depth: 3


Annonce par Xavier
==================

.. seealso::

   - http://redmine


::

    Sujet:  Mise en place Redmine
    Date :  Wed, 28 Nov 2012 17:56:41 +0100
    De :    Xavier CHOPIN <xavier.chopin@id3.eu>
    Organisation :  id3 Semiconductors
    Pour :  tout_id3 <tout_id3@id3.eu>


Bonjour à tous,

Redmine est un outil de gestion de projets nouvellement mis en place et
proposant un certains nombre de fonctionnalités:

* gestion multi-projets,
* gestion  des droits utilisateurs,
* rapports de bogues (bugs), demandes d'évolutions,
* Wiki multi-projets,
* forums multi-projets,
* news accessibles par RSS / ATOM,
* notifications par courriel,
* gestion de feuilles de route, GANTT, calendrier,
* historique,
* intégration avec divers suivis de versions:

  - SVN
  - CVS,
  - Mercurial,
  - Git,
  - Bazaar
  - Darcs.

Dans la 1ere phase de déploiement, je souhaite que chacun se connecte à
l'outil afin que je puisse créer les droits correspondant à votre profil.

Adresse de l'outil : http://redmine

Pour les utilisateurs de Firefox, avant de se connecter au site,
veuillez cliquer sur:

  * Outils -> Préférences -> Avancé -> Réseau -> Paramètres
  * Dans le champ "Pas de proxy", veuillez ajouter redmine


Merci,

Réponse
========

::

    Sujet: Re: Mise en place Redmine
    Date : Thu, 29 Nov 2012 08:29:05 +0100
    De : Patrick VERGAIN <patrick.vergain@id3.eu>
    Organisation : id3 Technologies
    Pour : Xavier CHOPIN <xavier.chopin@id3.eu>


::

    >
    > Dans la 1ere phase de déploiement, je souhaite que chacun se connecte à
    > l'outil afin que je puisse créer les droits correspondant à votre profil.


OK, c'est fait.

::


    >
    > Adresse de l'outil : http://redmine
    >
    > Pour les utilisateurs de Firefox, avant de se connecter au site,
    > veuillez cliquer sur:
    >
    >   * Outils -> Préférences -> Avancé -> Réseau -> Paramètres


Sur Firefox 17.0 Windows j'ai cliqué sur:

* Outils -> Options -> Paramètres (Connexion)

.. figure:: outils_options_parametres.png
   :align: center




