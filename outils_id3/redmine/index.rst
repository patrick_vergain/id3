
.. index::
   pair: Outil; Redmine


.. _redmine:

=================================================
Redmine
=================================================

.. seealso::

   - http://www.redmine.org/
   - http://en.wikipedia.org/wiki/Redmine
   - http://redmine

.. contents::
   :depth: 8


Annonce
========

.. toctree::
   :maxdepth: 3

   annonce

Redmine id3
============

.. seealso::

   - http://redmine



Introduction
============

Redmine est une application web libre de gestion presque complète de projet
en mode web, développé en Ruby sur la base du framework Ruby on Rails.

La gestion des tests devra être faite avec un autre outil.

Il a été créé par Jean-Philippe Lang.

D'autres développeurs venant de la communauté des utilisateurs de Redmine
contribuent depuis au projet.


Syntaxe de balisage
===================

Redmine utilise la syntaxe Textile pour ses pages de wiki et de nombreux autres
emplacements où l'utilisateur peut entrer du texte :

- descriptif de bogue ou de demande, ainsi que les commentaires associés
- forums
- nouvelles
- description accompagnant un fichier lié à un projet


Projects
========

.. toctree::
   :maxdepth: 3
   
   projects/index
   
   
   




