

.. index::
   pair: Bio ;  Actus (2014 22 janvier)


.. _bio_forums_discussions_generales_actus_22_janvier_2014:

============================================================
Biométrie Discussions générales Actualités 22 janvier 2014
============================================================

.. seealso::

   - http://redmine/boards/19/topics/24
   
   

QiSQi intègre id3Finger Toolkit 2.1
====================================


La société QiSQi (http://www.qisqi.com/) est en train d'intégrer id3Finger 
Toolkit 2.1 dans sa solution de finance participative (crowdfunding) :

    http://www.wetradenet.com/

Le site est basé sur le framework Vaadin (https://vaadin.com/home) et appellera 
les méthodes Java du SDK pour effectuer l'authentification du client avec notamment:

- côté client (Windows majoritairement):

    - la capture des empreintes digitales (DP4500, FS80)
    - l'extraction des minuties
    - l'enrôlement des 10 doigts avec contrôle des doublons (matching 1:10)
    
- côté serveur (Linux 64 bits):

    - la comparaison 1:1 avec le doigt spécifié


   
      






