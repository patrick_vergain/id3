

.. index::
   pair: Biométrie ;  OpenSC


.. _opensc_ref:

=================================================
OpenSC
=================================================

.. seealso::

   - http://redmine/boards/31/topics/64
   - https://github.com/OpenSC/OpenSC/wiki
   

Description
===========

OpenSC_ provides a set of libraries and utilities to work with smart cards. 

Its main focus is on cards that support cryptographic operations, and facilitate 
their use in security applications such as authentication, mail encryption and 
digital signatures. 

OpenSC implements the PKCS#11 API so applications supporting this API (such as 
Mozilla Firefox and Thunderbird) can use it. 

On the card OpenSC implements the PKCS#15 standard and aims to be compatible 
with every software/card that does so, too.

`Guide d'initialisation et de personnalisation de carte`_


.. _OpenSC:  https://github.com/OpenSC/OpenSC/wiki
.. _`Guide d'initialisation et de personnalisation de carte`:  https://www.opensc-project.org/opensc/wiki/CardPersonalization#Addaprivatekeycertificatesinapkcs12file

