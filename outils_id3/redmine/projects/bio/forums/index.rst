

.. index::
   pair: Bio; Forums


.. _bio_forums:

=================================================
Biométrie forums (boards)
=================================================

.. seealso::

   - http://redmine/projects/bio
   
   
.. toctree::
   :maxdepth: 8
   
   discussions_generales/index
   technologies/index
   
   
   




