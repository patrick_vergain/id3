
.. index::
   pair: Python ; Anaconda
   ! Anaconda


.. _python_anaconda3:

=================================
Python Anaconda 3
=================================

.. seealso::

   - https://www.continuum.io/downloads


.. contents::
   :depth: 3

Description
===========

The Anaconda platform provides an enterprise-ready data analytics platform that 
empowers companies to adopt a modern open data science analytics architecture.  

Processing multi-workload data analytics – from batch through interactive to 
real-time – the platform is used for both ad hoc and production deployments.


Bibliothèques 
==============

.. toctree::
   :maxdepth: 3
   
   bokeh/bokeh
   pandas/pandas
   
   
   
   
