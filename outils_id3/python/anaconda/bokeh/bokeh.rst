
.. index::
   pair: Visualization; Bokeh


.. _python_bokeh:

=================================
Python bokeh
=================================


.. contents::
   :depth: 3


Installation de bokeh
======================

::


    [pandas_35] C:\projects\jupyter\notebook1>conda install bokeh


::

    Fetching package metadata: ....
    Solving package specifications: ................................
    Package plan for installation in environment C:\Anaconda3\envs\pandas_35:

    The following NEW packages will be INSTALLED:

        bokeh:        0.10.0-py35_0
        flask:        0.10.1-py35_1
        itsdangerous: 0.24-py35_0
        pyyaml:       3.11-py35_2
        requests:     2.8.1-py35_0
        werkzeug:     0.10.4-py35_0


    Proceed ([y]/n)? y


::


    Extracting packages ...
    [      COMPLETE      ]|##################################################| 100%
    Linking packages ...
    [      COMPLETE      ]|##################################################| 100%
