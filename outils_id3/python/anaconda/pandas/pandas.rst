
.. index::
   pair: Data; Pandas 
   pair: Python ; Python 3.5


.. _pandas_35:

=================================
Python 3.5  pandas, seaborn
=================================


.. contents::
   :depth: 3
   

Création d'un environnement virtuel pandas_35
==============================================


::

    C:\projects_id3>conda create -n pandas_35 pandas
    
::
    
    Fetching package metadata: ....
    Solving package specifications: ............
    Package plan for installation in environment C:\Anaconda3\envs\pandas_35:

    The following packages will be downloaded:

        package                    |            build
        ---------------------------|-----------------
        msvc_runtime-1.0.0         |                0         573 KB
        pytz-2015.7                |           py35_0         167 KB
        ------------------------------------------------------------
                                               Total:         740 KB

    The following NEW packages will be INSTALLED:

        msvc_runtime:    1.0.0-0
        numpy:           1.10.1-py35_0
        pandas:          0.17.0-np110py35_0
        pip:             7.1.2-py35_0
        python:          3.5.0-3
        python-dateutil: 2.4.2-py35_0
        pytz:            2015.7-py35_0
        setuptools:      18.4-py35_0
        six:             1.10.0-py35_0
        wheel:           0.26.0-py35_1

    Proceed ([y]/n)? y

::


    Fetching packages ...
    msvc_runtime-1 100% |###############################| Time: 0:00:18  31.22 kB/s
    pytz-2015.7-py 100% |###############################| Time: 0:00:00 273.21 kB/s
    Extracting packages ...
    [      COMPLETE      ]|##################################################| 100%
    Linking packages ...
    [      COMPLETE      ]|##################################################| 100%
    #
    # To activate this environment, use:
    # > activate pandas_35
    #

::

    C:\projects_id3>activate pandas_35
    
::
    
    Activating environment "C:\Anaconda3\envs\pandas_35"...


Installation de jupyter
=======================

    [pandas_35] C:\projects_id3>conda install jupyter
    
::
    
    Fetching package metadata: ....
    Solving package specifications: .........................................................
    Package plan for installation in environment C:\Anaconda3\envs\pandas_35:

    The following packages will be downloaded:

        package                    |            build
        ---------------------------|-----------------
        sip-4.16.9                 |           py35_2         229 KB
        pyqt-4.11.4                |           py35_3         3.8 MB
        ------------------------------------------------------------
                                               Total:         4.1 MB

    The following NEW packages will be INSTALLED:

        decorator:        4.0.4-py35_0
        ipykernel:        4.1.1-py35_0
        ipython:          4.0.0-py35_1
        ipython_genutils: 0.1.0-py35_0
        ipywidgets:       4.1.0-py35_0
        jinja2:           2.8-py35_0
        jpeg:             8d-vc14_0     [vc14]
        jsonschema:       2.4.0-py35_0
        jupyter:          1.0.0-py35_0
        jupyter_client:   4.1.1-py35_0
        jupyter_console:  4.0.3-py35_0
        jupyter_core:     4.0.6-py35_0
        libpng:           1.6.17-vc14_1 [vc14]
        libsodium:        1.0.3-0
        libtiff:          4.0.6-vc14_0  [vc14]
        markupsafe:       0.23-py35_0
        mistune:          0.7.1-py35_0
        nbconvert:        4.0.0-py35_0
        nbformat:         4.0.1-py35_0
        notebook:         4.0.6-py35_0
        openssl:          1.0.2d-vc14_0 [vc14]
        path.py:          8.1.2-py35_0
        pickleshare:      0.5-py35_0
        pygments:         2.0.2-py35_0
        pyqt:             4.11.4-py35_3
        pyreadline:       2.1-py35_0
        pyzmq:            14.7.0-py35_1
        qt:               4.8.7-vc14_4  [vc14]
        qtconsole:        4.1.0-py35_0
        simplegeneric:    0.8.1-py35_0
        sip:              4.16.9-py35_2
        tornado:          4.2.1-py35_1
        traitlets:        4.0.0-py35_0
        zeromq:           4.1.3-vc14_1  [vc14]
        zlib:             1.2.8-vc14_2  [vc14]

    The following packages will be UPDATED:

        msvc_runtime:     1.0.0-0 --> 1.0.1-vc14_0  [vc14]

    Proceed ([y]/n)? y


::

    Fetching packages ...
    sip-4.16.9-py3 100% |###############################| Time: 0:00:01 234.03 kB/s
    pyqt-4.11.4-py 100% |###############################| Time: 0:00:09 440.17 kB/s
    Extracting packages ...
    [      COMPLETE      ]|##################################################| 100%
    Unlinking packages ...
    [      COMPLETE      ]|##################################################| 100%
    Linking packages ...
            1 fichier(s) copié(s).########################                   |  63%
    [      COMPLETE      ]|##################################################| 100%


Installation de seaborn
=======================

::

    [pandas_35] C:\projects_id3>conda install seaborn
    
::
    
    Fetching package metadata: ....
    Solving package specifications: ...................................
    Package plan for installation in environment C:\Anaconda3\envs\pandas_35:

    The following packages will be downloaded:

        package                    |            build
        ---------------------------|-----------------
        seaborn-0.6.0              |      np110py35_0         112 KB

    The following NEW packages will be INSTALLED:

        matplotlib: 1.4.3-np110py35_3
        pyparsing:  2.0.3-py35_0
        scipy:      0.16.0-np110py35_0
        seaborn:    0.6.0-np110py35_0
        tk:         8.5.18-vc14_0      [vc14]

    Proceed ([y]/n)? y


::

    Fetching packages ...
    seaborn-0.6.0- 100% |###############################| Time: 0:00:00 271.98 kB/s
    Extracting packages ...
    [      COMPLETE      ]|##################################################| 100%
    Linking packages ...
    [      COMPLETE      ]|##################################################| 100%


