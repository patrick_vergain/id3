"""

"""

import fire

import textwrap


def wrap(input_file):
    """Format a text to 80 characters.

    - the first step is to remove the line feeds.
    - the second step is to format line to 80 characters.
    - the third step is to remove extra spaces
    
    Calling example:
    
    
    """
    with open(input_file, 'r') as f:
        text = f.read()


    # remove the line feeds
    long_text = text.rstrip()
    
    # format line to 80 characters
    long_text_string = textwrap.fill(long_text, 80)
    for espaces in [
        '     ',
        '    ',
        '   ',
        '  ',
    ]:
        # remove extra spaces
        long_text_string = long_text_string.replace(espaces, ' ')
        
    print(long_text_string)


if __name__ == '__main__':
    fire.Fire()
