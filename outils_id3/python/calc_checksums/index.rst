
.. index::
   pair: calc_checksums; Tool


.. _calc_checksums:

=================================================
Script  ``calc_checksums``
=================================================


.. contents::
   :depth: 3

Mode d'emploi
=============

Se mettre sous l'environnement ``pvutils``::

    workon pvutils
    

Help
====


::

    (pvutils) calc_checksums.exe
    usage: %prog [options]

    Calculate the checksums of files under a directory
           [-h] [-l dir] [-p PATTERNS]

    optional arguments:
      -h, --help            show this help message and exit
      -l dir, --local_directory dir
                            The directory to scan
      -p PATTERNS, --patterns PATTERNS
                            Les patterns pour les fichiers: *.bat,*.exe,*.dll,*.zi
                            p,*.jar,*.egg,*.pdf,*.hex,*.cat,*.inf,*.sys,*.lib,*.rc
                            ,*.res,*.p7b,*.def,*.cer,*.bin,*.msi,*.wsi,*.h,*.a,*.s
                            o,*.wsq,*.bmp,*.jp2,*.png,*.jpg,*.tif,*.raw,*.tar,*.jf
                            fs2,*.patch,*.bz2,*.gz,*.py,*.pl,*.sh,*.pyc,*.ko
          
       

Exemple d'emploi
================

.. toctree::
   :maxdepth: 3
   
   exemples/index
   
   

