
.. index::
   pair: Communication ; Hipchat
   pair: Hipchat ; id3teamdevops
   ! id3teamdevops


.. _hipchat:

=================================================
Hipchat
=================================================

.. seealso::

   - http://en.wikipedia.org/wiki/HipChat
   - https://blog.hipchat.com
   - https://twitter.com/HipChat
   
  
id3teamdevops
=============
  
.. seealso::

   - https://id3teamdevops.hipchat.com/chat   


.. figure:: id3teamdevops.png
   :align: center
   
   
.. figure:: use_case_hipchat.png
   :align: center   
   

Welcome! Send this link to coworkers who need accounts: 
https://www.hipchat.com/invite/302792/f4a102c99d245f7d279f1f1fd51476c0
