
.. index::
   pair: Outil; Labview

.. _labview_tool:

=================================================
Labview
=================================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Labview
   - http://www.ni.com/labview/

.. contents::
   :depth: 3


Introduction
============

LabVIEW (contraction de Laboratory Virtual Instrument Engineering Workbench)
est le cœur d’une plate-forme de conception de systèmes de mesure et de
contrôle, basée sur un environnement de développement graphique de
National Instruments.

Le langage graphique utilisé dans cette plate-forme est appelé "G".

Créé à l’origine sur Apple Macintosh en 1986, LabVIEW est utilisé
principalement pour la mesure par acquisition de données, pour le contrôle
d’instruments et pour l’automatisme industriel.

La plate-forme de développement s’exécute sous différents systèmes
d’exploitation comme Microsoft Windows, Linux et Mac OS X.

LabVIEW peut générer du code sur ces systèmes d’exploitation mais
également sur des plates-formes temps réel, des systèmes embarqués ou
des composants reprogrammables FPGA.

La dernière version de LabVIEW est sortie en Août 2012.
