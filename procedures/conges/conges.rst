
.. index::
   pair: Sylvie; 06 48 81 07 27
   pair: Procédure ; Congés
   ! rh@id3.eu


.. _procedure_absence:

=================================================
PROCÉDURE DE DEMANDE D'ABSENCE (CP PVE)
=================================================


.. contents::
   :depth: 3


Message de Sylvie
====================

::

    -------- Forwarded Message --------
    Subject: 	Note de service : PROCÉDURE DE DEMANDE D'ABSENCE
    Date: 	Wed, 20 Feb 2019 18:19:58 +0100
    From: 	Sylvie CERIOLO <sylvie.ceriolo@id3.eu>
    To: 	tout_id3 <tout_id3@id3.eu>


PROCÉDURE DE DEMANDE D'ABSENCE :

absence : congé payé, rtt, récup, événement familial...ou même absence temporaire
ou bureau pendant les heures de présence obligatoire.

si absence non prévue : merci soit d'appeler Sylvie ou SMS au 06 48 81 07 27

Vos demandes d'absence par courriel uniquement :

- elles doivent être adressées à votre responsable de service et en copie
  au service RH sur l'adresse : rh@id3.eu

- Sujet : CP et vos initiales (sur 3 lettres)  quelque soit le motif
  (et rien d'autres !)
  Ex : CP SCE, CP PVE


- le contenu de mail est libre, mais doit être explicite.

Votre responsable de service:

- 1) vous répond (refus ou acceptation) MAIS l'absence de réponse vaut refus
- 2) en l'absence du responsable de service ou bien si il y a un problème, le
  service RH vous répond (refus ou acceptation).
