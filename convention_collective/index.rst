
.. index::
   pair: Convention collective; Métallurgie ingénieur et cadres
   pair: Convention collective; 650


.. _convention_collective:

=========================================================
Convention collective Métallurgie : ingénieurs et cadres
=========================================================

.. seealso:: http://www.legifrance.gouv.fr/affichIDCC.do?idConvention=KALICONT000005635842


:Identifiant Convention collective: 650
:Brochure n°: 3025


- Convention collective nationale du 13 mars 1972

- Convention collective nationale des ingénieurs et cadres de la métallurgie
  du 13 mars 1972. Etendue par arrêté du 27 avril 1973 (JO du 29 mai 1973)



