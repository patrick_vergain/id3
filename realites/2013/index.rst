
.. index::
   pair: id3 Technologies ; Réalité
   pair: SSII; ESN
   pair: Syntec ; Informatique
   pair: Syntec ; Numérique


.. _realites_metier_dev_2013:

================================================================
Réalités des métiers concernant les nouvelles technologies 2013
================================================================


.. seealso::

   - http://business.lesechos.fr/directions-ressources-humaines/partenaire/surcharge-de-travail-attention-danger-9388.php


.. contents::
   :depth: 3




SSII , société de prostitution néo-libérale légale? - Partie I 
=================================================================

.. seealso::

   - http://blogs.mediapart.fr/blog/mathieu/240212/ssii-societe-de-prostitution-neo-liberale-legale-partie-i



https://speakerdeck.com/mrjmad/deprime-au-bord-du-burn-out-et-pourtant-il-faut-continuer-a-coder



http://www.espace-chsct.fr/toutes-les-actualites/1342-stress-au-travail--une-l-faute-inexcusable-de-lemployeur-r.html


Stress au travail : une « faute inexcusable de l’employeur »
=============================================================

.. seealso::

   - http://www.espace-chsct.fr/toutes-les-actualites/1342-stress-au-travail--une-l-faute-inexcusable-de-lemployeur-r.html


Un stress analysé comme étant la conséquence d’un accident du travail peut 
désormais invoquer la « faute inexcusable » de son employeur, selon un arrêt 
de la Cour de cassation. 

En cas d’accident suivi de mort, les ayants droit du salarié peuvent demander 
à l’employeur réparation du préjudice moral devant la juridiction compétente.




Déprimé, au bord du burn-out, et pourtant il faut continuer à coder
====================================================================


.. seealso::

   - https://speakerdeck.com/mrjmad/deprime-au-bord-du-burn-out-et-pourtant-il-faut-continuer-a-coder

Published June 24, 2013 in Programming

Une conf qui se base sur mon expérience personnelle de "sur-travail". 

Comment on en arrive à se faire mal à la santé, comment on sort de cette spirale, 
comment on ne rechute pas, voila tout ce que je voulais traiter dans cette conf.

Cette conf a été faite à Pytong 2013, lors d'une session du Plug, aux 
RMLL Bruxelles et en plénière PyconFR 2013 Strasbourg.




SSII : un nouveau nom pour attirer les diplômés : ESN ( Entreprises de services du numérique)
=============================================================================================


.. seealso:: 

   - http://www.letudiant.fr/educpros/actualite/ne-dites-plus-ssii-mais-esn.html


Depuis le 11 avril 2013, les SSII (Sociétés de services en ingénierie informatique) 
n’existent plus. 

Le Syntec Numérique, syndicat professionnel du secteur, vient de les rebaptiser. 

Désormais, il faudra parler des ESN, Entreprises de services du numérique.


Le sigle risque de rester encore quelque temps dans l'esprit des étudiants. 

Les SSII, principal employeur des jeunes diplômés ingénieurs dans les TIC 
(technologies de l'information et de la communication), changent de nom, à 
l'initiative du Syntec Numérique.

La reprise des recrutements dans les SSII, le rapport de l'IGAS sur l'APEC, 
l'entreprise du futur_REVUE DE PRESSE DE L'EMPLOI 07.06.2010

Lors de la présentation semestrielle des chiffres du secteur le 11 avril 2013, 
le syndicat professionnel a annoncé la nouvelle identité des sociétés de services 
en ingénierie informatique. 
Elles deviennent Entreprises de services du numérique (ESN).
Un nouveau nom pour une autre image

"En France, le terme 'informatique' est chargé négativement, contrairement à 
d'autres pays où il est porteur d'innovation et de dynamisme, constate Pascal Brier, 
administrateur du Syntec Numérique. 

Il y a un an, nous avons transformé notre nom, Syntec Informatique devenant 
Syntec Numérique. Notre pouvoir d'attractivité a changé, car le numérique 
englobe bien plus d'activités que l'informatique. 
C'est cette même problématique qui a nourri notre réflexion pour les SSII."

A la demande des entreprises, le syndicat a donc travaillé à l'élaboration 
d'un nouveau vocable. 
Le but est clair : changer l’image du secteur et le rendre plus attractif, 
dans un contexte où les entreprises cherchent fortement à recruter. 
"Pour les jeunes, l’informatique est aujourd’hui encore très liée aux activités 
techniques, à l’image du geek, constate Noël Bouffard, directeur délégué de 
l’ESN/SSII Sopra Group. 
Or, les personnes qui travaillent dans le numérique sont bien plus que des 
techniciens."


