

.. index::
   pair: Glossaire ; id3


.. _glossaire:


=============================
Glossaire id3
=============================


.. glossary::

   id3
       id3 Technologies est une société d’étude spécialisée dans la conception
       et le développement de composants, de systèmes électroniques et
       d'applications dans les domaines de la Radiofréquence, de la Biométrie
       et du Sans contact (RFID).

       .. seealso:: http://www.id3.eu/

   PCB
   Circuit imprimé
       http://fr.wikipedia.org/wiki/Circuit_imprim%C3%A9
       http://en.wikipedia.org/wiki/Printed_circuit_board

       Un circuit imprimé (le sigle PCB de l'expression en anglais
       « Printed Circuit Board » est également utilisé) est un support, en
       général une plaque, permettant de relier électriquement un ensemble de
       composants électroniques entre eux, dans le but de réaliser un circuit
       électronique complexe. On le désigne aussi par le terme de carte électronique.

       Il est constitué d'un assemblage d'une ou plusieurs fines couches de
       cuivre séparées par un matériau isolant. Les couches de cuivre sont
       gravées par un procédé chimique pour obtenir un ensemble de pistes,
       terminées par des pastilles.

       Le circuit imprimé est souvent recouvert d'une couche de vernis coloré
       qui protège les pistes de l'oxydation et d'éventuels courts-circuits.

       Les pistes relient électriquement différentes zones du circuit imprimé.

       Les pastilles, une fois perforées, établissent une liaison électrique,
       soit entre les composants soudés à travers le circuit imprimé, soit entre
       les différentes couches de cuivre.
       Dans certains cas, des pastilles non perforées servent à souder des
       composants montés en surface


.. contents::
   :depth: 3



Glossaires sécurité
===================

Sécurité et identité
--------------------


:download:`Télécharger le glossaire sécurité <Glossaire SecuriteetIdentite.pdf>`


un document bien utile pour connaitre ou se remémorer la signification
de moults abréviations utilisées dans nos métiers

* EDF par exemple signifie European Data Format
* ECC : european citizen card mais aussi Elliptic curve cryptography
